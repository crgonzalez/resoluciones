package cl.mutual.bac.mgr;



import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.websphere.cache.DistributedMap;

public class MgrDynacacheBAC {

	private static final Logger logger = LogManager.getLogger(MgrDynacacheBAC.class);
	
	private static final String SPACE_UNDERSCORD = "_";
	private static final String SERVICES_CACHE_USER_CACHE = "services/cache/UserCache";

	private String requestSessionId;
	
	public MgrDynacacheBAC(String requestSessionId) {
		super();
		this.requestSessionId = requestSessionId;
	}

	public DistributedMap init() throws PortletException {
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			DistributedMap cacheObject = (DistributedMap) ctx.lookup(SERVICES_CACHE_USER_CACHE);
			cacheObject.setTimeToLive(-1);
			return cacheObject;
		} catch (NamingException e) {
			logger.error("[init] error.", e);
		}
		return null;
	}

	public String getObjeto(String nomObjeto) throws PortletException{
		String response = null;
		try{
			DistributedMap cacheObject = this.init();
			if (cacheObject != null) {
				nomObjeto = new StringBuffer(requestSessionId).append(SPACE_UNDERSCORD).append(nomObjeto).toString();
				if (cacheObject.containsKey(nomObjeto)) {
					response = (String) cacheObject.get(nomObjeto);
				}
			}
		} catch(Exception e){
			logger.error("[getObjeto] error.", e);
		}
		return response;
	}

}
