package cl.mutual.bac.mgr;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.portlet.PortletPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cl.mutual.bac.MTResolucionBACPortletSessionBean;
import cl.mutual.bac.util.Utilidades;
import cl.mutual.bac.vo.Centro;
import cl.mutual.bac.vo.Elemento;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MgrServiciosBAC {
	
	private PortletPreferences prefs;

	public MgrServiciosBAC(PortletPreferences prefs) {
		super();
		this.prefs = prefs;
	}

	private static final Logger logger = LogManager.getLogger(MgrServiciosBAC.class);
	
	public List<Elemento> getRegiones() {
		List<Elemento> regiones = new ArrayList<Elemento>();
		JSONObject json = new JSONObject();
		try {
			RequestConfig.Builder requestBuilder = RequestConfig.custom();
			requestBuilder = requestBuilder.setConnectTimeout(120000);
			requestBuilder = requestBuilder.setConnectionRequestTimeout(120000);

			HttpClientBuilder builder = HttpClientBuilder.create();
			builder.setDefaultRequestConfig(requestBuilder.build());
			HttpClient client = builder.build();

			String urlLlamada = prefs.getValue("url-regiones", "");

			logger.debug("Llamando a servicio: " + urlLlamada);

			HttpPost post = new HttpPost(urlLlamada);
			post.addHeader("Content-Type", "application/json");
			post.setEntity(new StringEntity(json.toString(), "UTF8"));
			
			HttpResponse response = client.execute(post);
			String output = EntityUtils.toString(response.getEntity());

			output = new String(output.getBytes(Charset.forName("UTF8")), "UTF8");
			logger.debug("regiones: " + (output != null && output.length() > 255 ? output.substring(0, 255) + "..." : output));
			JSONArray jsonObjectArray = new JSONArray(output);

			if (jsonObjectArray != null) {

				for (int i = 0; i < jsonObjectArray.length(); i++) {
					JSONObject jsonObject = jsonObjectArray.getJSONObject(i);
					Elemento elemento = new Elemento();
					if (!jsonObject.isNull("codigo")) {
						elemento.setId(String.valueOf(Integer.parseInt(jsonObject.get("codigo").toString())));
					}
					if (!jsonObject.isNull("descripcion")) {
						elemento.setValor(Utilidades.getNormalizar((String) jsonObject.get("descripcion")));
					}
					regiones.add(elemento);
				}
			}

		} catch (Exception ex) {
			logger.error("Error obteniendo servicio:", ex);
		}
		if(regiones != null){
			Collections.sort(regiones,new Comparator<Elemento>() {
	            @Override
	            public int compare(Elemento obj1, Elemento obj2) {
	                if (obj1.equals(obj2)) {
	                    return 0;
	                }
	                try{
	                	return Integer.parseInt(String.valueOf(obj1.getId())) - Integer.parseInt(String.valueOf(obj2.getId()));
	                } catch(Exception ex){
	                	return 0;
	                }
	            }
	        });
		}
		
		return regiones;
	}
	
	public List<Elemento> getComunas(int idRegion) {
		List<Elemento> comunas = new ArrayList<Elemento>();

		JSONObject json = new JSONObject();
		try {
			RequestConfig.Builder requestBuilder = RequestConfig.custom();
			requestBuilder = requestBuilder.setConnectTimeout(120000);
			requestBuilder = requestBuilder.setConnectionRequestTimeout(120000);

			String region = "";
			if(idRegion < 10) {
				region = "0" + idRegion;
			} else {
				region = String.valueOf(idRegion);
			}
			
			json.put("idComuna", region);

			HttpClientBuilder builder = HttpClientBuilder.create();
			builder.setDefaultRequestConfig(requestBuilder.build());
			HttpClient client = builder.build();

			String urlLlamada = prefs.getValue("url-comunas", "");

			logger.debug("Llamando a servicio: " + urlLlamada);

			HttpPost post = new HttpPost(urlLlamada);
			post.addHeader("Content-Type", "application/json");
			post.setEntity(new StringEntity(json.toString(), "UTF8"));

			HttpResponse response = client.execute(post);
			String output = EntityUtils.toString(response.getEntity());

			output = new String(output.getBytes(Charset.forName("UTF8")), "UTF8");

			logger.debug("comunas: " + (output != null && output.length() > 255 ? output.substring(0, 255) + "..." : output));
			JSONArray jsonObjectArray = new JSONArray(output);

			if (jsonObjectArray != null) {

				for (int i = 0; i < jsonObjectArray.length(); i++) {
					JSONObject jsonObject = jsonObjectArray.getJSONObject(i);
					Elemento comuna = new Elemento();
					if (!jsonObject.isNull("codigo")) {
					comuna.setId((String) jsonObject.get("codigo"));
					}
					if (!jsonObject.isNull("descripcion")) {
						comuna.setValor(Utilidades.getNormalizar((String) jsonObject.get("descripcion")));
					}
					comuna.setIdPadre(String.valueOf(idRegion));
					comunas.add(comuna);
				}
			}

		} catch (Exception ex) {
			logger.error("Error obteniendo servicio:" + ex.getMessage());
		}

		return comunas;
	}
	
	public List<Centro> getCentrosTrabajo(int rutEmpresa, List<Elemento> regiones) {
		List<Centro> centros = new ArrayList<>();
		try {
			logger.info("getCentrosTrabajo: rutEmpresa:" + rutEmpresa);
			String urlLlamada = prefs.getValue("ct-url-centrostrabajo", "");
			logger.info("getCentrosTrabajo: urlLlamada:" + urlLlamada);
			urlLlamada = urlLlamada.replace("[RUT_EMPRESA]", String.valueOf(rutEmpresa));
			logger.info("getCentrosTrabajo Llamando a servicio GET: " + urlLlamada);
			OkHttpClient client = getClienteSSL();
			if (client != null) {
				Request requestWS = null;
				if (prefs.getValue("ct-Authorization", "") != null && !prefs.getValue("ct-Authorization", "").isEmpty()) {
					requestWS = new Request.Builder().url(urlLlamada).addHeader("Ocp-Apim-Subscription-Key", prefs.getValue("ct-Ocp-Apim-Subscription-Key", "")).addHeader("Authorization", prefs.getValue("ct-Authorization", "")).addHeader("accept", "application/json").get().build();
				} else {
					requestWS = new Request.Builder().url(urlLlamada).addHeader("Ocp-Apim-Subscription-Key", prefs.getValue("ct-Ocp-Apim-Subscription-Key", "")).addHeader("accept", "application/json").get().build();
				}

				Response resultObject = client.newCall(requestWS).execute();
				logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

				if (resultObject.code() == 200 || resultObject.code() == 201) {
					
					HashMap<String, String> hashRegiones = new HashMap<String, String>();
					for(Elemento e:regiones) {
						hashRegiones.put(e.getId(), e.getValor());
					}
					String output = resultObject.body().string();
					JSONObject raiz = new JSONObject(output);
					JSONArray dObject = raiz.getJSONArray("CentrosTrabajoMaestro");

					for (int i = 0; i < dObject.length(); i++) {
						JSONObject result = dObject.getJSONObject(i);
						try {
							String idCentroTrabajo = getObjetoStringJson(result, "IdCentroTrabajo");
							String nombreCalle = getObjetoStringJson(result, "NombreCalle");
							
							if(Utilidades.tieneDatos(nombreCalle) && !nombreCalle.equalsIgnoreCase("null")){
								Centro centro = new Centro();
								centro.setCentroIdInterno(idCentroTrabajo);
								centro.setCalle(nombreCalle);
								centro.setNumero(getObjetoStringJson(result, "NumeroCalle"));
								centro.setComunaId(getObjetoStringJson(result, "CodigoComuna"));
								centro.setComuna(getObjetoStringJson(result, "Comuna"));
								
								String idRegion = String.valueOf(Integer.parseInt(centro.getComunaId().substring(0, 2)));
								centro.setRegionId(idRegion);
								centro.setRegion(hashRegiones.get(idRegion));
								
								
								centro.setNombreCentro(getObjetoStringJson(result, "Nombre"));
								centro.setCentroId(Utilidades.getIdCentro(centro));
								centros.add(centro);
							}
	
						} catch(Exception exx) {
							logger.warn(exx);
						}
					}
				} else {
					logger.info(resultObject.body().string());
				}
			}
		} catch (Exception ex) {
			logger.error(ex);
		}
		if(centros != null){
			Collections.sort(centros,new Comparator<Centro>() {
	            @Override
	            public int compare(Centro obj1, Centro obj2) {
	                if (obj1.equals(obj2)) {
	                    return 0;
	                }
                	return obj1.getNombreCentro().compareTo(obj2.getNombreCentro());
	            }
	        });
		}
		return centros;
	}
	
	public void getInformacionUsuarioTds(MTResolucionBACPortletSessionBean sessionBean) {

		String urlServicio = prefs.getValue("srv-tds-consultaUsuario", "");
		logger.info("getInformacionUsuarioTds: urlServicio:" + urlServicio);
		try {
			URL url = new URL(urlServicio);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) connection;
			ByteArrayOutputStream bout = new ByteArrayOutputStream();

			StringBuilder sbEntrada = new StringBuilder();
			sbEntrada.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cl=\"http://cl.mutual.osb.tds.srv/\">");
			sbEntrada.append("<soapenv:Header/>");
			sbEntrada.append("<soapenv:Body>");
			sbEntrada.append("<cl:obtieneDatosUsuarioTDS>");
			sbEntrada.append("<request>");
			sbEntrada.append("<rutAdministrador></rutAdministrador>");
			sbEntrada.append("<rutUsuario>" + sessionBean.getRutUsuarioInt() + "</rutUsuario>");
			sbEntrada.append("</request>");
			sbEntrada.append("</cl:obtieneDatosUsuarioTDS>");
			sbEntrada.append("</soapenv:Body>");
			sbEntrada.append("</soapenv:Envelope>");

			String xmlInput = sbEntrada.toString();
			logger.debug(xmlInput);
			byte[] buffer = new byte[xmlInput.length()];
			buffer = xmlInput.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();
			httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			httpConn.setRequestProperty("SOAPAction", "obtieneDatosUsuarioTDS");
			httpConn.setRequestProperty("Connection", "Keep-Alive");
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			OutputStream out = httpConn.getOutputStream();
			out.write(b);
			out.close();
			InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
			BufferedReader in = new BufferedReader(isr);
			StringBuilder sb = new StringBuilder();
			String responseString = "";
			while ((responseString = in.readLine()) != null) {
				sb.append(responseString);
			}
			logger.debug(sb.toString());
			try {
				org.json.JSONObject xmlJSONObj = org.json.XML.toJSONObject(sb.toString());

				for (int i = 0; i < 10; i++) {
					if (xmlJSONObj.getJSONObject("S:Envelope").getJSONObject("S:Body").has("ns" + i + ":obtieneDatosUsuarioTDSResponse")) {
						org.json.JSONObject objetoContenido = xmlJSONObj.getJSONObject("S:Envelope").getJSONObject("S:Body").getJSONObject("ns" + i + ":obtieneDatosUsuarioTDSResponse").getJSONObject("response").getJSONObject("usuarios");
						StringBuilder nombreSB = new StringBuilder();
						nombreSB.append(objetoContenido.has("nombresUsuario") ? objetoContenido.getString("nombresUsuario") : "");
						nombreSB.append(" ");
						nombreSB.append(objetoContenido.has("apellidoPaterno") ? objetoContenido.getString("apellidoPaterno") : "");
						nombreSB.append(" ");
						nombreSB.append(objetoContenido.has("apellidoMaterno") ? objetoContenido.getString("apellidoMaterno") : "");
						
						sessionBean.setNombreResponsable(nombreSB.toString().trim());
						sessionBean.setNombreUsuario(objetoContenido.has("nombresUsuario") ? objetoContenido.getString("nombresUsuario") : "");
						sessionBean.setAppUsuario(objetoContenido.has("apellidoPaterno") ? objetoContenido.getString("apellidoPaterno") : "");
						sessionBean.setApmUsuario(objetoContenido.has("apellidoMaterno") ? objetoContenido.getString("apellidoMaterno") : "");
						sessionBean.setCorreoElectronico(objetoContenido.has("correoElectronico") ? objetoContenido.getString("correoElectronico") : "");
						sessionBean.setTelefonoContacto(objetoContenido.has("telefonoPersonal") ? String.valueOf(objetoContenido.get("telefonoPersonal")) : "");
						
					}
				}

			} catch (JSONException e) {
				logger.error(e);
			}

		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}
	}
	
	public void getSolicitudes(MTResolucionBACPortletSessionBean sessionBean) {

		String urlServicio = prefs.getValue("srv-solicitudes", "");
		logger.info("getSolicitudes: urlServicio:" + urlServicio);

		try {
			OkHttpClient client = this.getCliente();

			urlServicio = urlServicio + sessionBean.getNumContrato() + "/" + sessionBean.getRutUsuarioInt();

			Request requestWS = new Request.Builder().url(urlServicio).addHeader("Content-Type", "application/json").get().build();

			Response resultObject = client.newCall(requestWS).execute();
			logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

			if (resultObject.code() == 200 || resultObject.code() == 201 || resultObject.code() == 204) {
				String output = resultObject.body().string();
				logger.debug(output);
				
				JSONObject jsonObject = new JSONObject(output);
				if (jsonObject.has("solicitudes")) {
					JSONArray jsonArray = jsonObject.getJSONArray("solicitudes");
	
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObjectSolicitudes = jsonArray.getJSONObject(i);

						if (jsonObjectSolicitudes.has("razonSocial") && jsonObjectSolicitudes.get("razonSocial") != null) {
							sessionBean.setRazonSocial(Utilidades.getValorStringJson(jsonObjectSolicitudes, "razonSocial"));
							sessionBean.setSolicitudCreada(true);
						}
						StringBuilder nombreResponsable = new StringBuilder();
						if (jsonObjectSolicitudes.has("nombre") && jsonObjectSolicitudes.get("nombre") != null) {
							nombreResponsable.append(Utilidades.getValorStringJson(jsonObjectSolicitudes, "nombre") + " ");
							sessionBean.setNombreUsuario(Utilidades.getValorStringJson(jsonObjectSolicitudes, "nombre"));
						}
						if (jsonObjectSolicitudes.has("apePaterno") && jsonObjectSolicitudes.get("apePaterno") != null) {
							nombreResponsable.append(Utilidades.getValorStringJson(jsonObjectSolicitudes, "apePaterno") + " ");
							sessionBean.setAppUsuario(Utilidades.getValorStringJson(jsonObjectSolicitudes, "apePaterno"));
						} 
						if (jsonObjectSolicitudes.has("apeMaterno") && jsonObjectSolicitudes.get("apeMaterno") != null) {
							nombreResponsable.append(Utilidades.getValorStringJson(jsonObjectSolicitudes, "apeMaterno") + " ");
							sessionBean.setApmUsuario(Utilidades.getValorStringJson(jsonObjectSolicitudes, "apeMaterno"));
						}
						sessionBean.setNombreResponsable(nombreResponsable.toString().trim());
						if (jsonObjectSolicitudes.has("correo") && jsonObjectSolicitudes.get("correo") != null) {
							sessionBean.setCorreoElectronico(Utilidades.getValorStringJson(jsonObjectSolicitudes, "correo"));
						}
						if (jsonObjectSolicitudes.has("telefono") && jsonObjectSolicitudes.get("telefono") != null) {
							sessionBean.setTelefonoContacto(Utilidades.getValorStringJson(jsonObjectSolicitudes, "telefono"));
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}
	}
	
	public boolean postSolicitudes(MTResolucionBACPortletSessionBean sessionBean) {
		boolean proceso = false;
		String urlServicio = prefs.getValue("srv-solicitudes", "");
		logger.info("postSolicitudes: urlServicio:" + urlServicio);

		try {
			OkHttpClient client = this.getCliente();

			JSONObject requestJSON = new JSONObject();
			requestJSON.put("rutEmpresa", sessionBean.getRutEmpresaInt());
			requestJSON.put("dvEmpresa", Utilidades.getDVRut(sessionBean.getRutEmpresaInt()));
			requestJSON.put("razonSocial", sessionBean.getRazonSocial());
			requestJSON.put("contrato", sessionBean.getNumContrato());
			requestJSON.put("rutUsuario", sessionBean.getRutUsuarioInt());
			requestJSON.put("dvUsuario", Utilidades.getDVRut(sessionBean.getRutUsuarioInt()));
			requestJSON.put("nombre", sessionBean.getNombreUsuario());
			requestJSON.put("apePaterno", sessionBean.getAppUsuario());
			requestJSON.put("apeMaterno", sessionBean.getApmUsuario());
			requestJSON.put("telefono", sessionBean.getTelefonoContacto());
			requestJSON.put("correo", sessionBean.getCorreoElectronico());
			
			RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestJSON.toString());

			Request requestWS = new Request.Builder().url(urlServicio).addHeader("Content-Type", "application/json").post(body).build();

			Response resultObject = client.newCall(requestWS).execute();
			logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

			if (resultObject.code() == 200 || resultObject.code() == 201 || resultObject.code() == 204) {
				proceso = true;
			}
			String output = resultObject.body().string();
			logger.debug(output);
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}

		return proceso;
	}
	
	public boolean putSolicitudes(MTResolucionBACPortletSessionBean sessionBean) {
		boolean proceso = false;
		String urlServicio = prefs.getValue("srv-solicitudes", "");
		
		urlServicio = urlServicio + sessionBean.getNumContrato() + "/" + sessionBean.getRutUsuarioInt();
		
		logger.info("putSolicitudes: urlServicio:" + urlServicio);

		try {
			OkHttpClient client = this.getCliente();

			JSONObject requestJSON = new JSONObject();
			requestJSON.put("rutEmpresa", sessionBean.getRutEmpresaInt());
			requestJSON.put("dvEmpresa", Utilidades.getDVRut(sessionBean.getRutEmpresaInt()));
			requestJSON.put("razonSocial", sessionBean.getRazonSocial());
			requestJSON.put("dvUsuario", Utilidades.getDVRut(sessionBean.getRutUsuarioInt()));
			requestJSON.put("nombre", sessionBean.getNombreUsuario());
			requestJSON.put("apePaterno", sessionBean.getAppUsuario());
			requestJSON.put("apeMaterno", sessionBean.getApmUsuario());
			requestJSON.put("telefono", sessionBean.getTelefonoContacto());
			requestJSON.put("correo", sessionBean.getCorreoElectronico());
			
			RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestJSON.toString());

			Request requestWS = new Request.Builder().url(urlServicio).addHeader("Content-Type", "application/json").put(body).build();

			Response resultObject = client.newCall(requestWS).execute();
			logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

			if (resultObject.code() == 200 || resultObject.code() == 201 || resultObject.code() == 204) {
				proceso = true;
			}
			String output = resultObject.body().string();
			logger.debug(output);
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}

		return proceso;
	}
	
	public void getResolucion(MTResolucionBACPortletSessionBean sessionBean) {

		String urlServicio = prefs.getValue("srv-resolucion", "");
		logger.info("getResolucion: urlServicio:" + urlServicio);

		try {
			OkHttpClient client = this.getCliente();

			urlServicio = urlServicio + sessionBean.getNumContrato() + "/" + sessionBean.getRutUsuarioInt();

			Request requestWS = new Request.Builder().url(urlServicio).addHeader("Content-Type", "application/json").get().build();

			Response resultObject = client.newCall(requestWS).execute();
			logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

			if (resultObject.code() == 200 || resultObject.code() == 201 || resultObject.code() == 204) {
				String output = resultObject.body().string();
				logger.debug(output);
				
				JSONObject jsonObject = new JSONObject(output);
				
				Set<String> centrosBD = new HashSet<String>();
				for(Centro ctBD : sessionBean.getCentrosBD()) {
					centrosBD.add(ctBD.getCentroId());
				}
				
				sessionBean.setCentros(new ArrayList<Centro>());
				
				if (jsonObject.has("sucursales")) {
					JSONArray jsonArray = jsonObject.getJSONArray("sucursales");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObjectSolicitudes = jsonArray.getJSONObject(i);
						
						if (jsonObjectSolicitudes.has("estado") && jsonObjectSolicitudes.get("estado") != null) {
							String estado = Utilidades.getValorStringJson(jsonObjectSolicitudes, "estado");
							if(!estado.equals("2")) {
								
								Centro centro = new Centro();
							
								if (jsonObjectSolicitudes.has("correlativo") && jsonObjectSolicitudes.get("correlativo") != null) {
									centro.setCentroIdInterno(Utilidades.getValorStringJson(jsonObjectSolicitudes, "correlativo"));
								}
								if (jsonObjectSolicitudes.has("nombreCentro") && jsonObjectSolicitudes.get("nombreCentro") != null) {
									centro.setNombreCentro(Utilidades.getValorStringJson(jsonObjectSolicitudes, "nombreCentro"));
								}
								if (jsonObjectSolicitudes.has("calle") && jsonObjectSolicitudes.get("calle") != null) {
									centro.setCalle(Utilidades.getValorStringJson(jsonObjectSolicitudes, "calle"));
								}
								if (jsonObjectSolicitudes.has("numero") && jsonObjectSolicitudes.get("numero") != null) {
									centro.setNumero(Utilidades.getValorStringJson(jsonObjectSolicitudes, "numero"));
								}
								if (jsonObjectSolicitudes.has("oficina") && jsonObjectSolicitudes.get("oficina") != null) {
									centro.setDepto(Utilidades.getValorStringJson(jsonObjectSolicitudes, "oficina"));
								}
								if (jsonObjectSolicitudes.has("region") && jsonObjectSolicitudes.get("region") != null) {
									String region = Utilidades.getValorStringJson(jsonObjectSolicitudes, "region");
									centro.setRegionId(region.split("\\;")[0]);
									if(region.split("\\;").length > 1)
										centro.setRegion(region.split("\\;")[1]);
								}
								if (jsonObjectSolicitudes.has("comuna") && jsonObjectSolicitudes.get("comuna") != null) {
									String comuna = Utilidades.getValorStringJson(jsonObjectSolicitudes, "comuna");
									centro.setComunaId(comuna.split("\\;")[0]);
									if(comuna.split("\\;").length > 1)
										centro.setComuna(comuna.split("\\;")[1]);
								}
								centro.setCentroId(Utilidades.getIdCentro(centro));
								
								int rutUsuarioCentro = 0;
								if (jsonObjectSolicitudes.has("rutUsuario") && jsonObjectSolicitudes.get("rutUsuario") != null) {
									rutUsuarioCentro = Integer.parseInt(Utilidades.getValorStringJson(jsonObjectSolicitudes, "rutUsuario"));
								}
								centro.setAsignado(true);
								centro.setCentroOtros(false);
								
								if(centrosBD.contains(centro.getCentroId())) {
									centro.setEditable(false);
									centro.setCentroOtros(false);
								} else {						
									centro.setCentroOtros(true);
								}
								
								if(rutUsuarioCentro != 0 && rutUsuarioCentro == sessionBean.getRutUsuarioInt()) {
									if(centro.isCentroOtros()) {
										centro.setEditable(true);
									} else {
										centro.setEditable(true);
									}
									centro.setSoloLectura(false);
								} else {
									centro.setEditable(false);
									centro.setSoloLectura(true);
								}
								
								sessionBean.getCentros().add(centro);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}
	}
	
	public boolean postResolucion(MTResolucionBACPortletSessionBean sessionBean, Centro centro) {
		boolean proceso = false;
		String urlServicio = prefs.getValue("srv-resolucion", "");
		logger.info("postResolucion: urlServicio:" + urlServicio);
		logger.info(String.format("agregando centro %s", centro));
		
		try {
			OkHttpClient client = this.getCliente();

			JSONObject requestJSON = new JSONObject();
			requestJSON.put("contrato", sessionBean.getNumContrato());
			requestJSON.put("rutUsuario", sessionBean.getRutUsuarioInt());
			requestJSON.put("nombreCentro", centro.getNombreCentro());
			requestJSON.put("calle", centro.getCalle());
			requestJSON.put("numero", centro.getNumero());
			requestJSON.put("oficina", centro.getDepto());
			requestJSON.put("region", centro.getRegionId().concat(";").concat(centro.getRegion()));
			requestJSON.put("ciudad", "");
			requestJSON.put("comuna", centro.getComunaId().concat(";").concat(centro.getComuna()));
			requestJSON.put("estado", "1");
			logger.debug(requestJSON);
			RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestJSON.toString());

			Request requestWS = new Request.Builder().url(urlServicio).addHeader("Content-Type", "application/json").post(body).build();

			Response resultObject = client.newCall(requestWS).execute();
			logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

			if (resultObject.code() == 200 || resultObject.code() == 201 || resultObject.code() == 204) {
				proceso = true;
			} else {
			}
			String output = resultObject.body().string();
			logger.debug(output);
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}

		return proceso;
	}
	
	public boolean deleteResolucion(MTResolucionBACPortletSessionBean sessionBean, String centroId) {

		String urlServicio = prefs.getValue("srv-resolucion", "");
		logger.info("getResolucion: urlServicio:" + urlServicio);
		logger.info(String.format("eliminando centro %s", centroId));
		
		try {
			OkHttpClient client = this.getCliente();

			urlServicio = urlServicio + centroId;

			Request requestWS = new Request.Builder().url(urlServicio).addHeader("Content-Type", "application/json").delete().build();

			Response resultObject = client.newCall(requestWS).execute();
			logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

			if (resultObject.code() == 200 || resultObject.code() == 201 || resultObject.code() == 204) {
				String output = resultObject.body().string();
				logger.debug(output);
				return true;
			}
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}
		return false;
	}
	
	public boolean putResolucion(MTResolucionBACPortletSessionBean sessionBean, Centro centro) {
		boolean proceso = false;
		String urlServicio = prefs.getValue("srv-resolucion", "");
		
		urlServicio = urlServicio + sessionBean.getNumContrato();
		
		logger.info("putResolucion: urlServicio:" + urlServicio);
		logger.info(String.format("actualizando centro %s", centro));
		
		try {
			OkHttpClient client = this.getCliente();

			JSONObject requestJSON = new JSONObject();
			requestJSON.put("nombreCentro", centro.getNombreCentro());
			requestJSON.put("calle", centro.getCalle());
			requestJSON.put("numero", centro.getNumero());
			requestJSON.put("oficina", centro.getDepto());
			requestJSON.put("region", centro.getRegionId().concat(";").concat(centro.getRegion()));
			requestJSON.put("ciudad", "");
			requestJSON.put("comuna", centro.getComunaId().concat(";").concat(centro.getComuna()));
			requestJSON.put("estado", "1");
			
			RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestJSON.toString());

			Request requestWS = new Request.Builder().url(urlServicio).addHeader("Content-Type", "application/json").put(body).build();

			Response resultObject = client.newCall(requestWS).execute();
			logger.info("Response:" + resultObject.code() + ":" + resultObject.message());

			if (resultObject.code() == 200 || resultObject.code() == 201 || resultObject.code() == 204) {
				proceso = true;
			} else {
			}
			String output = resultObject.body().string();
			logger.debug(output);
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}

		return proceso;
	}
	
	private OkHttpClient getCliente() {
		try {
			OkHttpClient.Builder build = new OkHttpClient.Builder();
			return build.connectTimeout(120, TimeUnit.SECONDS).writeTimeout(120, TimeUnit.SECONDS).readTimeout(120, TimeUnit.SECONDS).build();
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}
		return null;
	}
	
	private OkHttpClient getClienteSSL() {
		String tls = prefs.getValue("srv-SSLContext", "");
		try {
			final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
					logger.debug("checkClientTrusted");
				}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
					logger.debug("checkServerTrusted");
				}

				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return new java.security.cert.X509Certificate[] {};
				}
			} };
			final SSLContext sslContext = SSLContext.getInstance(tls);
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

			final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

			OkHttpClient.Builder build = new OkHttpClient.Builder();
			build.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
			build.hostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return hostname != null;
				}
			});
			return build.connectTimeout(120, TimeUnit.SECONDS).writeTimeout(120, TimeUnit.SECONDS).readTimeout(120, TimeUnit.SECONDS).build();
		} catch (Exception ex) {
			logger.error("Hubo un error en el proceso", ex);
		}
		return null;
	}
	
	private String getObjetoStringJson(JSONObject result, String tipo) {
		try {
			if (result.get(tipo) != null)
				return result.get(tipo).toString();
		} catch (Exception ex) {
			if (logger.isDebugEnabled())
				logger.warn(ex.getMessage());
		}
		return "";
	}
	
}
