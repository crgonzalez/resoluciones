package cl.mutual.bac.mgr;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.portlet.PortletPreferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * clase de envio de correos
 * 
 * @author jdelgado
 *
 */
public class MgrCorreosBAC {

	private static Logger logger = LogManager.getLogger(MgrCorreosBAC.class);

	private PortletPreferences prefs;
	
	public MgrCorreosBAC(PortletPreferences prefs) {
		super();
		this.prefs = prefs;
	}
	
	public boolean enviaNotificaciones(String html, String correos) {
		try {
			correos = correos.replace("|", ",");
			javax.naming.Context ctx = new javax.naming.InitialContext();
	        Session mailSession =  (Session)ctx.lookup(prefs.getValue("correo-jndi-was", ""));
	        MimeMessage message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(prefs.getValue("correo-mail-from", "")));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(correos));
			message.setSubject(prefs.getValue("correo-mail-asunto", ""), "utf-8");
			logger.info("asunto correo:"+message.getSubject());
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(html, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			message.setContent(multipart);

			Transport.send(message);
			return true;
		} catch (Exception ex) {
			logger.error(ex);
		}
		return false;
	}

}
