package cl.mutual.bac.vo;

public class Elemento {

	private String valor;
	private String id;
	private String idPadre;

	public Elemento() {
		super();
	}

	public Elemento(String valor, String id) {
		super();
		this.valor = valor;
		this.id = id;
	}

	public Elemento(String valor, String id, String idPadre) {
		super();
		this.valor = valor;
		this.id = id;
		this.idPadre = idPadre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}

	@Override
	public String toString() {
		return "Elemento [valor=" + valor + ", id=" + id + ", idPadre=" + idPadre + "]";
	}

}
