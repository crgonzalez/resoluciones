package cl.mutual.bac.vo;

public class Centro {

	private String centroId = "";
	private String centroIdInterno = "";
	private String nombreCentro = "";
	private String calle = "";
	private String numero = "";
	private String depto = "";
	private String regionId = "";
	private String region = "";
	private String ComunaId = "";
	private String comuna = "";
	private boolean soloLectura = false;
	private boolean editable = false;
	private boolean asignado = false;
	private boolean centroOtros = false;

	public String getCentroId() {
		return centroId;
	}

	public void setCentroId(String centroId) {
		this.centroId = centroId;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDepto() {
		return depto;
	}

	public void setDepto(String depto) {
		this.depto = depto;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getComunaId() {
		return ComunaId;
	}

	public void setComunaId(String comunaId) {
		ComunaId = comunaId;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public boolean isSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(boolean soloLectura) {
		this.soloLectura = soloLectura;
	}

	public String getCentroIdInterno() {
		return centroIdInterno;
	}

	public void setCentroIdInterno(String centroIdInterno) {
		this.centroIdInterno = centroIdInterno;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public String getNombreCentro() {
		return nombreCentro;
	}

	public void setNombreCentro(String nombreCentro) {
		this.nombreCentro = nombreCentro;
	}

	public boolean isAsignado() {
		return asignado;
	}

	public void setAsignado(boolean asignado) {
		this.asignado = asignado;
	}

	public boolean isCentroOtros() {
		return centroOtros;
	}

	public void setCentroOtros(boolean centroOtros) {
		this.centroOtros = centroOtros;
	}

	@Override
	public String toString() {
		return "Centro [centroId=" + centroId + ", centroIdInterno=" + centroIdInterno + ", nombreCentro=" + nombreCentro + ", calle=" + calle + ", numero=" + numero + ", depto=" + depto + ", regionId=" + regionId + ", region=" + region + ", ComunaId=" + ComunaId + ", comuna=" + comuna + ", soloLectura=" + soloLectura + ", editable=" + editable + ", asignado=" + asignado + ", centroOtros=" + centroOtros + ", getCentroId()=" + getCentroId() + ", getCalle()=" + getCalle() + ", getNumero()=" + getNumero() + ", getDepto()=" + getDepto() + ", getRegionId()=" + getRegionId() + ", getRegion()=" + getRegion() + ", getComunaId()=" + getComunaId() + ", getComuna()=" + getComuna() + ", isSoloLectura()=" + isSoloLectura() + ", getCentroIdInterno()=" + getCentroIdInterno() + ", isEditable()=" + isEditable() + ", getNombreCentro()=" + getNombreCentro() + ", isAsignado()=" + isAsignado() + ", isCentroOtros()=" + isCentroOtros() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
