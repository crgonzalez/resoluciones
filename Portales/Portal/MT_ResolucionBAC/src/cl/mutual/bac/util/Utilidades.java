package cl.mutual.bac.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

import javax.portlet.PortletPreferences;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.json.JSONObject;

import cl.mutual.bac.vo.Centro;

public class Utilidades {

	private static final Logger logger = LogManager.getLogger(Utilidades.class);

	public static void configLogger(String nivelLog) {

		Level level = Level.INFO;
		if (nivelLog.equalsIgnoreCase("ALL")) {
			level = Level.ALL;
		} else if (nivelLog.equalsIgnoreCase("DEBUG")) {
			level = Level.DEBUG;
		} else if (nivelLog.equalsIgnoreCase("ERROR")) {
			level = Level.ERROR;
		} else if (nivelLog.equalsIgnoreCase("FATAL")) {
			level = Level.FATAL;
		} else if (nivelLog.equalsIgnoreCase("WARN")) {
			level = Level.WARN;
		} else if (nivelLog.equalsIgnoreCase("TRACE")) {
			level = Level.TRACE;
		}
		Configurator.setRootLevel(level);

	}

	public static String getPreferencia(PortletPreferences prefs, String valor) {
		return (String) prefs.getValue(valor, "");
	}

	public static boolean tieneDatos(String str) {
		return str != null && !str.trim().isEmpty() && !str.trim().equalsIgnoreCase("null");
	}

	public static String getTextoNoNulo(String str) {
		if (tieneDatos(str)) {
			return str.trim();
		}
		return "";
	}
	
	public static String getIdCentro(Centro centro) {
		StringBuilder id =  new StringBuilder();
		if(centro != null) {
			id.append(centro.getComunaId());
			id.append(centro.getCalle());
			id.append(centro.getNumero());
		}
		String retorno = id.toString();
		retorno = retorno.replace(" ", "");
		retorno = retorno.replace(",", "");
		retorno = retorno.replace(".", "");
		retorno = retorno.replace("'", "");
		retorno = retorno.replace("\"", "");
		retorno = retorno.replace("$", "");
		retorno = retorno.replace("|", "");
		retorno = retorno.replace("/", "");
		return retorno;
		
	}
	
	public static String getRutFormateado(int strInt) {
		if (strInt == 0)
			return null;
		String str = getRutCompleto(strInt);
		str = str.replaceAll("\\.", "");
		str = str.replaceAll("\\-", "");
		str = str.trim().toUpperCase();

		if (str.length() < 2)
			return null;
		try {
			Long _rut = Long.parseLong(str.substring(0, str.length() - 1));
			String _dv = str.substring(str.length() - 1, str.length());

			DecimalFormat df = new DecimalFormat("###,###.##", new java.text.DecimalFormatSymbols(new java.util.Locale("es")));
			String rut = df.format(_rut) + "-" + _dv;

			return rut.toUpperCase();
		} catch (Exception ex) {
			return null;
		}
	}
	
	public static String getDVRut(int rut) {

		try {
			int M = 0, S = 1, T = rut;
			for (; T != 0; T /= 10)
				S = (S + T % 10 * (9 - M++ % 6)) % 11;
			String dv = String.valueOf((char) (S != 0 ? S + 47 : 75));
			return dv;

		} catch (Exception ex) {
			;
		}

		return null;
	}
	
	public static String getRutCompleto(int rut) {
		try {
			return rut + "-" + getDVRut(rut);
		} catch (Exception ex) {
			;
		}
		return null;
	}
	
	public static String getValorStringJson(JSONObject jsonObject, String valor) {
		if (jsonObject.has(valor) && jsonObject.get(valor) != null) {
			String retorno = jsonObject.get(valor).toString();
			if(!retorno.equalsIgnoreCase("null")) {
				return retorno;
			}
		}
		return "";
	}
	
	public static String obtenerHtml(String componente) {
		StringBuilder sb = new StringBuilder();
		try {
            URL url = new URL(componente);
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            br.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return sb.toString();
	}
	
	
	public static String getNormalizar(String str) {
		if (str != null) {	
			
			str = str.replace("Ñ","&#209;");
			str = str.replace("É","&#201;");
			str = str.replace("Ó","&#211");
			str = str.replace("�","&#205;");
			
			
			str = str.replace("�","&#192;");
			str = str.replace("�","&#193;");
			str = str.replace("�","&#194;");
			//str = str.replace("�","&#195;");
			str = str.replace("�","&#196;");
			str = str.replace("�","&#197;");
			str = str.replace("�","&#198;");
			str = str.replace("�","&#199;");
			str = str.replace("�","&#200;");
			str = str.replace("�","&#201;");
			str = str.replace("�","&#202;");
			str = str.replace("�","&#203;");
			str = str.replace("�","&#204;");
			str = str.replace("�","&#205;");
			str = str.replace("�","&#206;");
			str = str.replace("�","&#207;");
			str = str.replace("�","&#208;");
			str = str.replace("�","&#209;");
			str = str.replace("�","&#210;");
			str = str.replace("�","&#211;");
			str = str.replace("�","&#212;");
			str = str.replace("�","&#213;");
			str = str.replace("�","&#214;");
			str = str.replace("�","&#215;");
			str = str.replace("�","&#216;");
			str = str.replace("�","&#217;");
			str = str.replace("�","&#218;");
			str = str.replace("�","&#219;");
			str = str.replace("�","&#220;");
			str = str.replace("�","&#221;");
			str = str.replace("�","&#222;");
			str = str.replace("�","&#223;");
			str = str.replace("�","&#224;");
			str = str.replace("�","&#225;");
			str = str.replace("�","&#226;");
			str = str.replace("�","&#227;");
			str = str.replace("�","&#228;");
			str = str.replace("�","&#229;");
			str = str.replace("�","&#230;");
			str = str.replace("�","&#231;");
			str = str.replace("�","&#232;");
			str = str.replace("�","&#233;");
			str = str.replace("�","&#234;");
			str = str.replace("�","&#235;");
			str = str.replace("�","&#236;");
			str = str.replace("�","&#237;");
			str = str.replace("�","&#238;");
			str = str.replace("�","&#239;");
			str = str.replace("�","&#240;");
			str = str.replace("�","&#241;");
			str = str.replace("�","&#242;");
			str = str.replace("�","&#243;");
			str = str.replace("�","&#244;");
			str = str.replace("�","&#245;");
			str = str.replace("�","&#246;");
			str = str.replace("�","&#247;");
			str = str.replace("�","&#248;");
			str = str.replace("�","&#249;");
			str = str.replace("�","&#250;");
			str = str.replace("�","&#251;");
			str = str.replace("�","&#252;");
			str = str.replace("�","&#253;");
			str = str.replace("�","&#254;");
			str = str.replace("�","&#255;");
			str = str.replace("�","&#338;");
			str = str.replace("�","&#339;");
			str = str.replace("�","&#352;");
			str = str.replace("�","&#353;");
			str = str.replace("�","&#376;");
			str = str.replace("�","&#402;");
			
			str = str.replaceAll("[^\\p{ASCII}]", "");
			

		} else {
			str = "";
		}
		return str;
	}

}
