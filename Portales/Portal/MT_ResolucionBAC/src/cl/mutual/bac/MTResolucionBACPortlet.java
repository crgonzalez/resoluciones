package cl.mutual.bac;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import cl.mutual.bac.mgr.MgrCorreosBAC;
import cl.mutual.bac.mgr.MgrDynacacheBAC;
import cl.mutual.bac.mgr.MgrServiciosBAC;
import cl.mutual.bac.util.Utilidades;
import cl.mutual.bac.vo.Centro;
import cl.mutual.bac.vo.Elemento;

/**
 * A sample portlet based on GenericPortlet
 */
public class MTResolucionBACPortlet extends GenericPortlet {

	private static final Logger logger = LogManager.getLogger(MTResolucionBACPortlet.class);
	
	public static final String JSP_FOLDER    = "/_MT_ResolucionBAC/jsp/";    // JSP folder name

	public static final String VIEW_JSP      = "MTResolucionBACPortletView";         // JSP file name to be rendered on the view mode
	public static final String SESSION_BEAN  = "MTResolucionBACPortletSessionBean";  // Bean name for the portlet session
	public static final String FORM_SUBMIT   = "formularioBAC";   // Action name for submit form



	 
	/**
	 * @see javax.portlet.Portlet#init()
	 */
	public void init() throws PortletException{
		super.init();
	}

	/**
	 * Serve up the <code>view</code> mode.
	 * 
	 * @see javax.portlet.GenericPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	public void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		// Set the MIME type for the render response
		response.setContentType(request.getResponseContentType());
		
		Utilidades.configLogger(Utilidades.getPreferencia(request.getPreferences(), "nivel-logs"));

		// Check if portlet session exists
		MTResolucionBACPortletSessionBean sessionBean = getSessionBean(request);
		if( sessionBean==null ) {
			response.getWriter().println("<b>NO PORTLET SESSION YET</b>");
			return;
		}
		
		/*int rutEmpresa = 76592971;
		int rutUsuario = 15318297;
		int numContrato = 260827;
		String razonSocial = "Flagare";*/
		int rutEmpresa = 0;
		int rutUsuario = 0;
		int numContrato = 0;
		String razonSocial = "";
				
		sessionBean.prefs = request.getPreferences();
		
		MgrServiciosBAC mgrServicios = new MgrServiciosBAC(sessionBean.prefs);
		MgrDynacacheBAC mgrDynacacheBAC = new MgrDynacacheBAC(request.getRequestedSessionId());
		try {
			rutEmpresa = Integer.parseInt(mgrDynacacheBAC.getObjeto("rutEmpresa"));
			numContrato = Integer.parseInt(mgrDynacacheBAC.getObjeto("numContrato"));
			razonSocial = mgrDynacacheBAC.getObjeto("razonSocial");
			String str1 = request.getRemoteUser().split("\\,")[0];
			rutUsuario = Integer.parseInt(str1.split("\\=")[1]);
		} catch(Exception ex) {
			logger.error(ex);
		}
		
		logger.info(String.format("Inicio de rutEmpresa[%s] rutUsuario[%s] numContrato[%s]", rutEmpresa, rutUsuario, numContrato));
		
		sessionBean.setRutaKitUI(Utilidades.getPreferencia(request.getPreferences(), "ruta-kit-ui"));
		sessionBean.setRutEmpresaInt(rutEmpresa);
		sessionBean.setRutEmpresa(Utilidades.getRutFormateado(rutEmpresa));
		sessionBean.setRutUsuarioInt(rutUsuario);
		sessionBean.setRutResponsable(Utilidades.getRutFormateado(rutUsuario));
		sessionBean.setRazonSocial(razonSocial);
		sessionBean.setNumContrato(numContrato);
		
		sessionBean.setRegiones(mgrServicios.getRegiones());
		sessionBean.setCentrosBD(mgrServicios.getCentrosTrabajo(rutEmpresa, sessionBean.getRegiones()));
		mgrServicios.getSolicitudes(sessionBean);
		
		if(!sessionBean.isSolicitudCreada()) {
			mgrServicios.getInformacionUsuarioTds(sessionBean);
		}
		
		
		
		mgrServicios.getResolucion(sessionBean);
		
		logger.debug(sessionBean);
		
		// Invoke the JSP to render
		PortletRequestDispatcher rd = getPortletContext().getRequestDispatcher(getJspFilePath(request, VIEW_JSP));
		rd.include(request,response);
	}

	/**
	 * Process an action request.
	 * 
	 * @see javax.portlet.Portlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	public void processAction(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException {
		
		// Set form text in the session bean
		MTResolucionBACPortletSessionBean sessionBean = getSessionBean(request);
		String telefonoUsuario = request.getParameter("telefonoContacto");
		String correoElectronico = request.getParameter("correoElectronico");
		String centrosFinales = request.getParameter("centrosFinales");
		
		sessionBean.setTelefonoContacto(telefonoUsuario);
		sessionBean.setCorreoElectronico(correoElectronico);
		
		logger.info(String.format("sessionBean %s", sessionBean));
		logger.info(String.format("centrosFinales %s", centrosFinales));
		
		HashMap<String, String> comunas = new HashMap<String, String>();
		
		Set<Centro> hashCentrosAgregar = new HashSet<Centro>();
		Set<Centro> hashCentrosEditar = new HashSet<Centro>();
		
		
		HashMap<String, Centro> hashActualCentrosBD = new HashMap<String, Centro>();
		HashMap<String, Centro> hashCentrosEliminar = new HashMap<String, Centro>();
		
		for(Centro centro:sessionBean.getCentrosBD()) {
			hashActualCentrosBD.put(centro.getCentroIdInterno(), centro);
		}
		
		for(Centro centro:sessionBean.getCentros()) {
			hashCentrosEliminar.put(centro.getCentroIdInterno(), centro);
		}
		
		
		
		String[] centros = centrosFinales.split("\\|");
		for(String centroStr : centros) {
			String[] centroArr = centroStr.split("\\,") ;
			if(centroArr.length > 1) {
				String centroId = centroArr[0];
				String centroIdInterno = centroArr[1];
				String centroNuevo = centroArr[2];
		 		String nombreCentro = centroArr[3];
		 		String calle = centroArr[4];
		 		String numero = centroArr[5];
		 		String depto = centroArr[6];
		 		String region = centroArr[7];
		 		String regionId = centroArr[8];
		 		String comuna = centroArr[9];
		 		String comunaId = centroArr[10];
				
		 		if(!centroNuevo.equals("0")) {
			 		
			 		if(!(centroIdInterno.equals("0") || centroIdInterno.isEmpty())) {
						if(hashActualCentrosBD.containsKey(centroIdInterno)) {				
							if(!hashCentrosEliminar.containsKey(centroIdInterno)) {
								Centro centro = hashActualCentrosBD.get(centroIdInterno);
								hashCentrosAgregar.add(centro);
							}
							hashCentrosEliminar.remove(centroIdInterno);
						}
			 		}
					
			 		
					if(centroNuevo.equals("2")) {
						Centro centro = new Centro();
			 			centro.setNombreCentro(nombreCentro);
			 			centro.setCalle(calle);
			 			centro.setNumero(numero);
			 			centro.setDepto(depto);
			 			centro.setRegionId(regionId);
			 			centro.setRegion(region);
			 			centro.setComunaId(comunaId);
			 			centro.setComuna(comuna);
				 		if(centroIdInterno.equals("0") || centroIdInterno.isEmpty()) {
				 			hashCentrosAgregar.add(centro);
				 		} else {
				 			centro.setCentroIdInterno(centroIdInterno);
				 			hashCentrosEditar.add(centro);
				 			hashCentrosEliminar.remove(centroIdInterno);
				 		}
					}
		 		} else {
		 			hashCentrosEliminar.remove(centroIdInterno);
		 		}
			}
		}
		
		HashMap<String, StringBuilder> hashCorreoRegiones = new HashMap<String, StringBuilder>();
		
		String html = Utilidades.obtenerHtml(sessionBean.prefs.getValue("correo-contenido", ""));
		String htmlInicio = "", htmlFin = "", htmlCentros = "";
		StringBuilder htmlFinal = new StringBuilder();
		htmlInicio = html.split("\\[CT\\_INICIO\\]")[0];
		htmlFin = html.split("\\[CT\\_FIN\\]")[1];
		
		htmlCentros = html.split("\\[CT\\_INICIO\\]")[1].split("\\[CT\\_FIN\\]")[0];
		
		MgrServiciosBAC mgrServicios = new MgrServiciosBAC(sessionBean.prefs);
		if(sessionBean.isSolicitudCreada()) {
			mgrServicios.putSolicitudes(sessionBean);
		} else {
			mgrServicios.postSolicitudes(sessionBean);
		}
		
		for(Centro centro:hashCentrosAgregar) {
			mgrServicios.postResolucion(sessionBean, centro);
			
			StringBuilder sb = null;
			if(hashCorreoRegiones.containsKey(centro.getRegionId())) {
				sb = hashCorreoRegiones.get(centro.getRegionId());
			} else {
				sb = new StringBuilder();
				String tmp = htmlInicio.replace("[REGION]", centro.getRegion());
				tmp = tmp.replace("[RAZON_SOCIAL]", sessionBean.getRazonSocial());
				sb.append(tmp);
				hashCorreoRegiones.put(centro.getRegionId(), sb);
			}
			String tmp = htmlCentros.replace("[NOM_CT]", centro.getNombreCentro());
			String direccion = centro.getCalle() + " " + centro.getNumero();
			if(Utilidades.tieneDatos(centro.getDepto())) {
				direccion = direccion + " " + centro.getDepto();
			}
			direccion = direccion + " " + centro.getComuna();
			tmp = tmp.replace("[DIRECCION_CT]", direccion);
			tmp = tmp.replace("[FECHA_CT]", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			
			sb.append(tmp);
			
			hashCorreoRegiones.put(centro.getRegionId(), sb);
			
		}
		
		for(Centro centro:hashCentrosEditar) {
			
			mgrServicios.putResolucion(sessionBean, centro);
			
			StringBuilder sb = null;
			if(hashCorreoRegiones.containsKey(centro.getRegionId())) {
				sb = hashCorreoRegiones.get(centro.getRegionId());
			} else {
				sb = new StringBuilder();
				String tmp = htmlInicio.replace("[REGION]", centro.getRegion());
				tmp = tmp.replace("[RAZON_SOCIAL]", sessionBean.getRazonSocial());
				sb.append(tmp);
				hashCorreoRegiones.put(centro.getRegionId(), sb);
			}
			String tmp = htmlCentros.replace("[NOM_CT]", centro.getNombreCentro());
			String direccion = centro.getCalle() + " " + centro.getNumero();
			if(Utilidades.tieneDatos(centro.getDepto())) {
				direccion = direccion + " " + centro.getDepto();
			}
			direccion = direccion + " " + centro.getComuna();
			tmp = tmp.replace("[DIRECCION_CT]", direccion);
			tmp = tmp.replace("[FECHA_CT]", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			tmp = tmp + "<br><br><hr>";
			sb.append(tmp);
			
			hashCorreoRegiones.put(centro.getRegionId(), sb);
		
		}
		
		for(String key:hashCentrosEliminar.keySet()) {
			Centro centro = hashCentrosEliminar.get(key);
			if(!centro.isSoloLectura()) {
				mgrServicios.deleteResolucion(sessionBean, centro.getCentroIdInterno());
			}
		}
		
		MgrCorreosBAC mgrCorreosBAC = new MgrCorreosBAC(sessionBean.prefs); 
		for(String key : hashCorreoRegiones.keySet()) {
			StringBuilder sb = hashCorreoRegiones.get(key);
			sb.append(htmlFin);
			String correos = sessionBean.prefs.getValue("correo-mail-destino-"+key, "");
			correos = correos.replace("|", ",");
			mgrCorreosBAC.enviaNotificaciones(sb.toString(), correos);
		}
	
	}
	
	/**
	 * Get SessionBean.
	 * 
	 * @param request PortletRequest
	 * @return MTResolucionBACPortletSessionBean
	 */
	private static MTResolucionBACPortletSessionBean getSessionBean(PortletRequest request) {
		PortletSession session = request.getPortletSession();
		if( session == null )
			return null;
		MTResolucionBACPortletSessionBean sessionBean = (MTResolucionBACPortletSessionBean)session.getAttribute(SESSION_BEAN);
		if( sessionBean == null ) {
			sessionBean = new MTResolucionBACPortletSessionBean();
			session.setAttribute(SESSION_BEAN,sessionBean);
		}
		return sessionBean;
	}

	/**
	 * Process a serve Resource request.
	 * 
	 * @see javax.portlet.Portlet#serveResource(javax.portlet.ResourceRequest, javax.portlet.ResourceResponse)
	 */
	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, java.io.IOException {
		String accion = getRequestParamValue(request, "accion");
		MTResolucionBACPortletSessionBean sessionBean = getSessionBean(request);
		logger.info("Recibiendo llamada ajax. accion:" + accion);
		if (accion != null) {
			String responseData = null;
			JSONObject jsonObj = new JSONObject();
			if(accion.equalsIgnoreCase("buscaComunas")){
				buscaComunas(request, jsonObj, sessionBean);
			} else if(accion.equalsIgnoreCase("agregaCentro")){
				agregaCentro(request, jsonObj, sessionBean);
			}
				
				
			responseData = jsonObj.toString();

			response.setContentType("application/json");
			PrintWriter pw = null;

			try {
				pw = response.getWriter();
				pw.write(responseData);
			} catch (Exception e) {
				logger.error("Hubo un error en el proceso", e);
			} finally {
				if (pw != null) {
					pw.close();
				}
			}
		}
	}
	
	private void buscaComunas(ResourceRequest request, JSONObject jsonObj, MTResolucionBACPortletSessionBean sessionBean){
		String region = getRequestParamValue(request, "region");
		logger.debug(String.format("busca comunas de region %s", region));
		if(region != null && !region.isEmpty()) {
			jsonObj.put("comunas", comunas(sessionBean, region));
			jsonObj.put("codigo", 0);
		}
	}
	
	private JSONArray comunas(MTResolucionBACPortletSessionBean sessionBean, String idRegion) {
		JSONArray jsonComunas = new JSONArray();
		logger.debug(String.format("busca comunas de region %s", idRegion));
		if(!idRegion.isEmpty()) {
			MgrServiciosBAC mgrServicios = new MgrServiciosBAC(sessionBean.prefs);
			List<Elemento> comunas = mgrServicios.getComunas(Integer.parseInt(idRegion));
			for(Elemento comuna:comunas) {
				JSONObject comunaJson = new JSONObject();
				comunaJson.put("id", comuna.getId());
				comunaJson.put("valor", comuna.getValor());
				jsonComunas.put(comunaJson);
			}
		}
		return jsonComunas;
	}
	
	private void agregaCentro(ResourceRequest request, JSONObject jsonObj, MTResolucionBACPortletSessionBean sessionBean){
		String centroId = getRequestParamValue(request, "centro");
		logger.debug(String.format("agregaCentro centro %s desde lista %s", centroId, sessionBean.getCentrosBD().size()));
		if(!centroId.isEmpty()) {
			JSONArray jsonComunas = new JSONArray();
			List<Centro> tmpListaCentros = new ArrayList<Centro>();
			for(Centro centro:sessionBean.getCentrosBD()) {
				if(centro.getCentroId().equalsIgnoreCase(centroId)) {
					JSONObject jsonCentro = new JSONObject();
					jsonCentro.put("calle", centro.getCalle());
					jsonCentro.put("centroId", centro.getCentroId());
					jsonCentro.put("centroIdInterno", centro.getCentroIdInterno());
					jsonCentro.put("comuna", centro.getComuna());
					jsonCentro.put("comunaId", centro.getComunaId());
					jsonCentro.put("depto", centro.getDepto());
					jsonCentro.put("nombreCentro", centro.getNombreCentro());
					jsonCentro.put("numero", centro.getNumero());
					jsonCentro.put("region", centro.getRegion());
					jsonCentro.put("regionId", centro.getRegionId());
					
					jsonObj.put("centro", jsonCentro);
					jsonObj.put("codigo", 0);
				}
			}
			
		}
	}
	
	/**
	 * Returns JSP file path.
	 * 
	 * @param request Render request
	 * @param jspFile JSP file name
	 * @return JSP file path
	 */
	private static String getJspFilePath(RenderRequest request, String jspFile) {
		String markup = request.getProperty("wps.markup");
		if( markup == null )
			markup = getMarkup(request.getResponseContentType());
		return JSP_FOLDER + markup + "/" + jspFile + "." + getJspExtension(markup);
	}

	/**
	 * Convert MIME type to markup name.
	 * 
	 * @param contentType MIME type
	 * @return Markup name
	 */
	private static String getMarkup(String contentType) {
		if( "text/vnd.wap.wml".equals(contentType) )
			return "wml";
        else
            return "html";
	}

	/**
	 * Returns the file extension for the JSP file
	 * 
	 * @param markupName Markup name
	 * @return JSP extension
	 */
	private static String getJspExtension(String markupName) {
		return "jsp";
	}
	
	public String getRequestParamValue(ResourceRequest request, String paramName) {
		String paramValue = null;
		if (paramName != null && request.getParameter(paramName) != null) {
			paramValue = (String) request.getParameter(paramName);
		}
		return paramValue;
	}

}
