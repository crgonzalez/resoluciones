//
// Generated By:JAX-WS RI IBM 2.2.1-11/28/2011 08:28 AM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package cl.mutual.bac.srv.seguimiento;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for realizarInscripcionCapacitacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="realizarInscripcionCapacitacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ingresoInscripcion" type="{http://cl.mutual.ws/}ingresoInscripcionVo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "realizarInscripcionCapacitacion", propOrder = {
    "ingresoInscripcion"
})
public class RealizarInscripcionCapacitacion
    implements Serializable
{

    protected IngresoInscripcionVo ingresoInscripcion;

    /**
     * Gets the value of the ingresoInscripcion property.
     * 
     * @return
     *     possible object is
     *     {@link IngresoInscripcionVo }
     *     
     */
    public IngresoInscripcionVo getIngresoInscripcion() {
        return ingresoInscripcion;
    }

    /**
     * Sets the value of the ingresoInscripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link IngresoInscripcionVo }
     *     
     */
    public void setIngresoInscripcion(IngresoInscripcionVo value) {
        this.ingresoInscripcion = value;
    }

}
