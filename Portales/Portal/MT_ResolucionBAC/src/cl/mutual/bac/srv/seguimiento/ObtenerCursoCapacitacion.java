//
// Generated By:JAX-WS RI IBM 2.2.1-11/28/2011 08:28 AM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package cl.mutual.bac.srv.seguimiento;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for obtenerCursoCapacitacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="obtenerCursoCapacitacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cursoId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numeroAdherente" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenerCursoCapacitacion", propOrder = {
    "cursoId",
    "numeroAdherente"
})
public class ObtenerCursoCapacitacion
    implements Serializable
{

    protected long cursoId;
    protected long numeroAdherente;

    /**
     * Gets the value of the cursoId property.
     * 
     */
    public long getCursoId() {
        return cursoId;
    }

    /**
     * Sets the value of the cursoId property.
     * 
     */
    public void setCursoId(long value) {
        this.cursoId = value;
    }

    /**
     * Gets the value of the numeroAdherente property.
     * 
     */
    public long getNumeroAdherente() {
        return numeroAdherente;
    }

    /**
     * Sets the value of the numeroAdherente property.
     * 
     */
    public void setNumeroAdherente(long value) {
        this.numeroAdherente = value;
    }

}
