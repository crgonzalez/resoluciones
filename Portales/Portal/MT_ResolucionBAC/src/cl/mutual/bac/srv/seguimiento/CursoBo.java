//
// Generated By:JAX-WS RI IBM 2.2.1-11/28/2011 08:28 AM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package cl.mutual.bac.srv.seguimiento;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for cursoBo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cursoBo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://cl.mutual.ws/}maestroBo">
 *       &lt;sequence>
 *         &lt;element name="cuposCurso" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cuposDisponibles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cursoId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="diasDuracion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="horaFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horaInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cursoBo", propOrder = {
    "cuposCurso",
    "cuposDisponibles",
    "cursoId",
    "diasDuracion",
    "direccion",
    "fecha",
    "horaFin",
    "horaInicio"
})
public class CursoBo
    extends MaestroBo
    implements Serializable
{

    protected int cuposCurso;
    protected int cuposDisponibles;
    protected long cursoId;
    protected int diasDuracion;
    protected String direccion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fecha;
    protected String horaFin;
    protected String horaInicio;

    /**
     * Gets the value of the cuposCurso property.
     * 
     */
    public int getCuposCurso() {
        return cuposCurso;
    }

    /**
     * Sets the value of the cuposCurso property.
     * 
     */
    public void setCuposCurso(int value) {
        this.cuposCurso = value;
    }

    /**
     * Gets the value of the cuposDisponibles property.
     * 
     */
    public int getCuposDisponibles() {
        return cuposDisponibles;
    }

    /**
     * Sets the value of the cuposDisponibles property.
     * 
     */
    public void setCuposDisponibles(int value) {
        this.cuposDisponibles = value;
    }

    /**
     * Gets the value of the cursoId property.
     * 
     */
    public long getCursoId() {
        return cursoId;
    }

    /**
     * Sets the value of the cursoId property.
     * 
     */
    public void setCursoId(long value) {
        this.cursoId = value;
    }

    /**
     * Gets the value of the diasDuracion property.
     * 
     */
    public int getDiasDuracion() {
        return diasDuracion;
    }

    /**
     * Sets the value of the diasDuracion property.
     * 
     */
    public void setDiasDuracion(int value) {
        this.diasDuracion = value;
    }

    /**
     * Gets the value of the direccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Sets the value of the direccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Gets the value of the fecha property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecha(XMLGregorianCalendar value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the horaFin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraFin() {
        return horaFin;
    }

    /**
     * Sets the value of the horaFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraFin(String value) {
        this.horaFin = value;
    }

    /**
     * Gets the value of the horaInicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraInicio() {
        return horaInicio;
    }

    /**
     * Sets the value of the horaInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraInicio(String value) {
        this.horaInicio = value;
    }

}
