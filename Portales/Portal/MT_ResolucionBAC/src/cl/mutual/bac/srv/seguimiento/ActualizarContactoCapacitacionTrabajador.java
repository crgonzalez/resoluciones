//
// Generated By:JAX-WS RI IBM 2.2.1-11/28/2011 08:28 AM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package cl.mutual.bac.srv.seguimiento;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for actualizarContactoCapacitacionTrabajador complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="actualizarContactoCapacitacionTrabajador">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ingresoResponsabeTrabajador" type="{http://cl.mutual.ws/}responsableTrabajadorVo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actualizarContactoCapacitacionTrabajador", propOrder = {
    "ingresoResponsabeTrabajador"
})
public class ActualizarContactoCapacitacionTrabajador
    implements Serializable
{

    protected ResponsableTrabajadorVo ingresoResponsabeTrabajador;

    /**
     * Gets the value of the ingresoResponsabeTrabajador property.
     * 
     * @return
     *     possible object is
     *     {@link ResponsableTrabajadorVo }
     *     
     */
    public ResponsableTrabajadorVo getIngresoResponsabeTrabajador() {
        return ingresoResponsabeTrabajador;
    }

    /**
     * Sets the value of the ingresoResponsabeTrabajador property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsableTrabajadorVo }
     *     
     */
    public void setIngresoResponsabeTrabajador(ResponsableTrabajadorVo value) {
        this.ingresoResponsabeTrabajador = value;
    }

}
