package cl.mutual.bac;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletPreferences;

import cl.mutual.bac.vo.Centro;
import cl.mutual.bac.vo.Elemento;

/**
 *
 * A sample Java bean that stores portlet instance data in portlet session.
 *
 */
public class MTResolucionBACPortletSessionBean {

	protected PortletPreferences prefs;
	
	private boolean solicitudCreada = false;
	private String rutaKitUI = "";
	private String razonSocial = "";
	private String rutEmpresa = "";
	private int numContrato = 0;
	private int rutEmpresaInt = 0;
	private int rutUsuarioInt = 0;
	private String nombreResponsable = "";
	private String nombreUsuario = "";
	private String appUsuario = "";
	private String apmUsuario = "";
	private String rutResponsable = "";
	private String telefonoContacto = "";
	private String correoElectronico = "";
	private String nombreCentro = "";
	private String centroId = "";
	private String calle = "";
	private String numero = "";
	private String depto = "";
	private String regionId = "";
	private String region = "";
	private String comunaId = "";
	private String comuna = "";

	private List<Elemento> regiones = new ArrayList<Elemento>();

	private List<Centro> centrosBD = new ArrayList<Centro>();
	private List<Centro> centros = new ArrayList<Centro>();
	
	public String getRutaKitUI() {
		return rutaKitUI;
	}

	public void setRutaKitUI(String rutaKitUI) {
		this.rutaKitUI = rutaKitUI;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRutEmpresa() {
		return rutEmpresa;
	}

	public void setRutEmpresa(String rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}

	public String getNombreResponsable() {
		return nombreResponsable;
	}

	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}

	public String getRutResponsable() {
		return rutResponsable;
	}

	public void setRutResponsable(String rutResponsable) {
		this.rutResponsable = rutResponsable;
	}

	public String getTelefonoContacto() {
		return telefonoContacto;
	}

	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDepto() {
		return depto;
	}

	public void setDepto(String depto) {
		this.depto = depto;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getComunaId() {
		return comunaId;
	}

	public void setComunaId(String comunaId) {
		this.comunaId = comunaId;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public String getCentroId() {
		return centroId;
	}

	public void setCentroId(String centroId) {
		this.centroId = centroId;
	}


	public List<Elemento> getRegiones() {
		return regiones;
	}

	public void setRegiones(List<Elemento> regiones) {
		this.regiones = regiones;
	}

	public String getNombreCentro() {
		return nombreCentro;
	}

	public void setNombreCentro(String nombreCentro) {
		this.nombreCentro = nombreCentro;
	}

	public List<Centro> getCentrosBD() {
		return centrosBD;
	}

	public void setCentrosBD(List<Centro> centrosBD) {
		this.centrosBD = centrosBD;
	}

	public List<Centro> getCentros() {
		return centros;
	}

	public void setCentros(List<Centro> centros) {
		this.centros = centros;
	}

	public int getNumContrato() {
		return numContrato;
	}

	public void setNumContrato(int numContrato) {
		this.numContrato = numContrato;
	}

	public int getRutEmpresaInt() {
		return rutEmpresaInt;
	}

	public void setRutEmpresaInt(int rutEmpresaInt) {
		this.rutEmpresaInt = rutEmpresaInt;
	}

	public int getRutUsuarioInt() {
		return rutUsuarioInt;
	}

	public void setRutUsuarioInt(int rutUsuarioInt) {
		this.rutUsuarioInt = rutUsuarioInt;
	}

	public boolean isSolicitudCreada() {
		return solicitudCreada;
	}

	public void setSolicitudCreada(boolean solicitudCreada) {
		this.solicitudCreada = solicitudCreada;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getAppUsuario() {
		return appUsuario;
	}

	public void setAppUsuario(String appUsuario) {
		this.appUsuario = appUsuario;
	}

	public String getApmUsuario() {
		return apmUsuario;
	}

	public void setApmUsuario(String apmUsuario) {
		this.apmUsuario = apmUsuario;
	}

	@Override
	public String toString() {
		return "MTResolucionBACPortletSessionBean [prefs=" + prefs + ", solicitudCreada=" + solicitudCreada + ", rutaKitUI=" + rutaKitUI + ", razonSocial=" + razonSocial + ", rutEmpresa=" + rutEmpresa + ", numContrato=" + numContrato + ", rutEmpresaInt=" + rutEmpresaInt + ", rutUsuarioInt=" + rutUsuarioInt + ", nombreResponsable=" + nombreResponsable + ", nombreUsuario=" + nombreUsuario + ", appUsuario=" + appUsuario + ", apmUsuario=" + apmUsuario + ", rutResponsable=" + rutResponsable + ", telefonoContacto=" + telefonoContacto + ", correoElectronico=" + correoElectronico + ", nombreCentro=" + nombreCentro + ", centroId=" + centroId + ", calle=" + calle + ", numero=" + numero + ", depto=" + depto + ", regionId=" + regionId + ", region=" + region + ", comunaId=" + comunaId + ", comuna=" + comuna + ", regiones=" + regiones + "]";
	}



}
