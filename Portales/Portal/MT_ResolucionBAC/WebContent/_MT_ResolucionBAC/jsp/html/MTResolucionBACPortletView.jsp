<%@page session="false" contentType="text/html" pageEncoding="ISO-8859-1" import="java.util.*,javax.portlet.*,cl.mutual.bac.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://www.ibm.com/xmlns/prod/websphere/portal/v6.1/portlet-client-model" prefix="portlet-client-model"%>
<portlet:defineObjects />
<portlet-client-model:init>
	<portlet-client-model:require module="ibm.portal.xml.*" />
	<portlet-client-model:require module="ibm.portal.portlet.*" />
</portlet-client-model:init>
<%
	cl.mutual.bac.MTResolucionBACPortletSessionBean sessionBean = (cl.mutual.bac.MTResolucionBACPortletSessionBean) renderRequest.getPortletSession().getAttribute(cl.mutual.bac.MTResolucionBACPortlet.SESSION_BEAN);
%>

<link rel="stylesheet" type="text/css" href="<%=sessionBean.getRutaKitUI()%>/latest/fonts/fonts.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<%=sessionBean.getRutaKitUI()%>/latest/components/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="<%=sessionBean.getRutaKitUI()%>/latest/mutual/semantic-mutual.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=sessionBean.getRutaKitUI()%>/latest/mutual/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=sessionBean.getRutaKitUI()%>/latest/components/tooltipster/css/tooltipster.bundle.min.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=sessionBean.getRutaKitUI()%>/latest/components/hc-offcanvas-nav.css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/estilos.css">

<script>
var centrosIngresados = [];
function arrAgregaCentro(centroId){
	centrosIngresados.push(centroId);
}

function arrExisteCentro(centroId){
	return centrosIngresados.includes(centroId);
}

function arrEliminaCentro(centroId){
	centrosIngresados = centrosIngresados.filter(function(item) {
	    return item !== centroId
	})
}
</script>
<body>


<div class="resolucionBAC">
	<div id="loadingwaitModal2" style="display: none;">
		<div class="ui active centered inline small loader"></div>
	</div>
	<div id="waitModal2"></div>
	
	<main id="container">

		<div class="ui segment content-segment">
			<h1 class="ui header">Resoluci�n BAC (B�squeda activa de casos)</h1>
			<p>La Superintendencia de Seguridad Social...</p>
			<div class="ui segment info-ex">
				<p>Desde ahora las entidades empleadoras...</p>
			</div>
	
			<div class="space"></div>
			
			<div class="ui equal width grid stackable">
				<div class="column">
					<form method="POST" id="formularioBAC" name="formularioBAC" class="ui segment form" action="<portlet:actionURL/>">
						<input type="hidden" value="" name="centrosFinales" id="centrosFinales"/>
						<input type="hidden" value="" name="centroIdInterno" id="centroIdInterno"/>
						<input type="hidden" value="" name="idCentroAccion" id="idCentroAccion">
						<h2 class="ui dividing header dark">Campos de identificaci�n de la empresa</h2>
						<div class="two fields">
							<div class="field ">
								<label class="tag">Raz�n Social</label>
								<div class="ui input">
									<input type="text" value="<%=sessionBean.getRazonSocial()%>" disabled>
								</div>
							</div>
							<div class="field ">
								<label class="tag">RUT Empresa</label>
								<div class="ui input">
									<input type="text" value="<%=sessionBean.getRutEmpresa()%>" disabled>
								</div>
							</div>
						</div>
						<div class="two fields">
							<div class="field ">
								<label class="tag">Nombre responsable de la informaci�n</label>
								<div class="ui input">
									<input type="text" value="<%=sessionBean.getNombreResponsable()%>" disabled>
								</div>
							</div>
							<div class="field ">
								<label class="tag">RUT Responsable</label>
								<div class="ui input">
									<input type="text" value="<%=sessionBean.getRutResponsable()%>" disabled>
								</div>
							</div>
						</div>
						<div class="two fields">
							<div class="field ">
								<label class="tag">* Tel�fono de contacto</label>
								<div class="ui labeled input ">
									<div class="ui label ">+56</div>
									<input type="text" placeholder="" name="telefonoContacto" id="telefonoContacto" value="<%=sessionBean.getTelefonoContacto()%>" maxlength="9" class="numerico input-phone input-activado">
								</div>
								<div class="input-error error-formulario error-formulario-telefono" style="display:none">
									<small class="mensaje-grid mens-invalido">
										<i class="icon error fas fa-times-circle"> </i>Debe ingresar el tel�fono de contacto
									</small>
								</div>
							</div>
							<div class="field ">
								<label class="tag">* Correo electr�nico</label>
								<div class="ui input">
									<input type="email" placeholder="" name="correoElectronico" id="correoElectronico" value="<%=sessionBean.getCorreoElectronico()%>" maxlength="100" class="correo">
								</div>
								<div class="input-error error-formulario error-formulario-correo" style="display:none">
									<small class="mensaje-grid mens-invalido">
										<i class="icon error fas fa-times-circle"> </i>Debe ingresar el correo electr�nico
									</small>
								</div>
							</div>
						</div>
							
						<h2 class="ui dividing header dark">Campos de datos de la b�squeda activa de casos</h2>
						<div class="two fields">
							<div class="field ">
								<label class="tag">Nombre del Centro de Trabajo</label>
								<div class="ui fluid  search selection dropdown">
									<input name="tags" type="hidden" name="centroTrabajoBD" id="centroTrabajoBD">
									 <i class="dropdown icon"></i>
									 <input class="search" autocomplete="off" tabindex="0">
									 <div class="default text">Seleccionar</div>
									 <div class="menu" tabindex="-1">
									 		<div class="item" data-value="0">- Otro -</div>
										 	<%for(cl.mutual.bac.vo.Centro centro:sessionBean.getCentrosBD()){ %>
										 		<div class="item" data-value="<%=centro.getCentroId()%>">
										 			<%=centro.getNombreCentro()%> - <%=centro.getCalle()%> <%=centro.getNumero()%> <%=centro.getComuna()%>
										 		</div>
										 	<%} %>
										 </div>
								 </div>
							</div>
							<div class="field " style="padding-top: 46px;">
								<button id="ingresarCentroTrabajo" type="button" class="ui secondary button">Ingresar Centro de Trabajo</button>
							</div>
						</div>
						
						<div id="centroTrabajoDetalle" style="display:none">
							<div class="two fields">
								<div class="two field ">
									<label class="tag">* Nombre</label>
									<div class="ui input">
										<input type="email" placeholder="" id="nombreCentro" name="nombreCentro" value="<%=sessionBean.getNombreCentro()%>" maxlength="100" class="alfanumerico">
									</div>
									<div class="input-error error-formulario error-formulario-nombreCentro" style="display:none">
										<small class="mensaje-grid mens-invalido">
											<i class="icon error fas fa-times-circle"> </i>Debe ingresar el nombre del centro
										</small>
									</div>
								</div>								
							</div>
							<div class="four fields">
								<div class="two field ">
									<label class="tag">* Calle</label>
									<div class="ui input">
										<input type="email" placeholder="" id="calle" name="calle" value="<%=sessionBean.getCalle()%>" maxlength="100" class="alfanumerico">
									</div>
									<div class="input-error error-formulario error-formulario-calle" style="display:none">
										<small class="mensaje-grid mens-invalido">
											<i class="icon error fas fa-times-circle"> </i>Debe ingresar la calle
										</small>
									</div>
								</div>
								<div class="field ">
									<label class="tag">* N�mero</label>
									<div class="ui input">
										<input type="email" placeholder="" id="numero" name="numero" value="<%=sessionBean.getNumero()%>" maxlength="15" class="alfanumerico">
									</div>
									<div class="input-error error-formulario error-formulario-numero" style="display:none">
										<small class="mensaje-grid mens-invalido">
											<i class="icon error fas fa-times-circle"> </i>Debe ingresar el n�mero de la calle
										</small>
									</div>
								</div>
								<div class="field ">
									<label class="tag">Depto/oficina</label>
									<div class="ui input">
										<input type="email" placeholder="" id="depto" name="depto" value="<%=sessionBean.getDepto()%>" maxlength="15" class="alfanumerico">
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field ">
									<label class="tag">* Regi�n</label>
									<div class="ui fluid  search selection dropdown">
										<input name="tags" type="hidden" id="region" name="region">
										 <i class="dropdown icon"></i>
										 <input class="search" autocomplete="off" tabindex="0">
										 <div class="default text">Seleccionar</div>
										 <div class="menu" tabindex="-1">
										 	<%for(cl.mutual.bac.vo.Elemento region:sessionBean.getRegiones()){ %>
										 		<div class="item" data-value="<%=region.getId()%>"><%=region.getValor() %></div>
										 	<%} %>
										 </div>
									 </div>
									 <div class="input-error error-formulario error-formulario-region" style="display:none">
										<small class="mensaje-grid mens-invalido">
											<i class="icon error fas fa-times-circle"> </i>Debe seleccionar la regi�n
										</small>
									</div>
								</div>
								<div class="field ">
									<label class="tag">* Comuna</label>
									<div class="ui fluid  search selection dropdown">
										<input name="tags" type="hidden" id="comuna" name="comuna">
										 <i class="dropdown icon"></i>
										 <input class="search" autocomplete="off" tabindex="0">
										 <div class="default text">Seleccionar</div>
										 <div class="menu" id="opcionesComunas" tabindex="-1">
										 </div>
									 </div>
									 <div class="input-error error-formulario error-formulario-comuna" style="display:none">
										<small class="mensaje-grid mens-invalido">
											<i class="icon error fas fa-times-circle"> </i>Debe seleccionar la comuna
										</small>
									</div>
								</div>
							</div>
							<div class="fields">
								<div class="field " style="padding-top: 46px;">
									<button id="cancelarCentro" type="button" class="ui secondary button medium" aria-label="Label">Cancelar</button>
									<button id="agregarCentro" type="button" class="ui primary button medium" aria-label="Label">Agregar</button>
									<button id="modificarCentro" type="button" class="ui primary button medium" aria-label="Label" style="display:none">Modificar</button>
								</div>
							</div>		
						</div>
						<div>					
							<div class="fields">
								<div class="field" style="width: 100%;">
									<h2 class="ui dividing header dark">Centros Seleccionados</h2>
									<div class="ui grid stackable">
										<div class="column">
											<div>												
												<table class="ui celled table tablaSemanticDefault" id="tablaCentros">
													<thead>
														<tr>
															<th class="sorting_disabled" rowspan="1" colspan="1">Nombre</th>
															<th class="sorting_disabled" rowspan="1" colspan="1">Calle</th>
															<th class="sorting_disabled" rowspan="1" colspan="1">N�mero</th>
															<th class="sorting_disabled" rowspan="1" colspan="1">Depto/Oficina</th>
															<th class="sorting_disabled" rowspan="1" colspan="1">Regi�n</th>
															<th class="sorting_disabled" rowspan="1" colspan="1">Comuna</th>
															<th class="sorting_disabled" rowspan="1" colspan="1">Acci�n</th>
														</tr>
													</thead>
													<tbody id="tbodyCentros">
														<%
														int contadorCentro = 0; 
														for(cl.mutual.bac.vo.Centro centro : sessionBean.getCentros()){ 
															contadorCentro++;
														%>
														<tr data-id="<%=centro.getCentroId()%>"
														data-centroIdInterno="<%=centro.getCentroIdInterno()%>" 
														data-centroNuevo="0"
														data-nombreCentro="<%=centro.getNombreCentro()%>"
														data-calle="<%=centro.getCalle()%>"
														data-numero="<%=centro.getNumero()%>"
														data-depto="<%=centro.getDepto()%>"
														data-regionId="<%=centro.getRegionId()%>"
														data-region="<%=centro.getRegion()%>"
														data-comuna="<%=centro.getComuna()%>"
														data-comunaId="<%=centro.getComunaId()%>"
														 >
															<td data-label="nombre">
																<div class="titleTable">Nombre :</div>
																<div class="tableData"><%=centro.getNombreCentro()%></div>
															</td>
															<td data-label="Calle">
																<div class="titleTable">Calle :</div>
																<div class="tableData"><%=centro.getCalle()%></div>
															</td>
															<td data-label="N�mero">
																<div class="titleTable">N�mero :</div>
																<div class="tableData"><%=centro.getNumero()%></div>
															</td>
															<td data-label="Depto/Oficina">
																<div class="titleTable">Depto/Oficina :</div>
																<div class="tableData"><%=centro.getDepto()%></div>
															</td>
															<td data-label="Regi�n">
																<div class="titleTable">Regi�n :</div>
																<div class="tableData"><%=centro.getRegion()%></div>
															</td>
															<td data-label="Comuna">
																<div class="titleTable">Comuna :</div>
																<div class="tableData"><%=centro.getComuna()%></div>
															</td>
															<td>
																<div class="titleTable">Acci�n</div>
																<div class="tableData">
																	<% if(!centro.isSoloLectura()){ %>
																		<% if(!centro.isEditable()){ %>
																		<span class="acciones accion-editar" data-id="<%=centro.getCentroId()%>" data-tooltip="Editar" data-position="top center">
																			<i class="icon far fa-edit link large"></i>
																		</span>
																		<%} %>
																		<span class="acciones accion-eliminar" data-id="<%=centro.getCentroId()%>" data-tooltip="Eliminar" data-position="top center">
																			<i class="icon far fa-trash link large"></i>
																		</span>
																	<%} %>
																	<script>
																		arrAgregaCentro('<%=centro.getCentroId()%>');																	
																	</script>
																</div>
															</td>
														</tr>
														<%} %>
													</tbody>
												</table>
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="fields">
								<div class="field " style="padding-top: 46px;">
									<button id="enviarCentros" type="button" class="ui primary button medium" aria-label="Label">Enviar Centros</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</main>
</div>

</body>

<script type="text/javascript">
	var resourceUrl = "<portlet:resourceURL/>";
</script>
<script src="<%=sessionBean.getRutaKitUI()%>/latest/jquery/jquery.min.js"></script>
<script src="<%=sessionBean.getRutaKitUI()%>/latest/jquery/jquery.rut.min.js"></script>
<script src="<%=sessionBean.getRutaKitUI()%>/latest/components/jquery.dataTables.min.js"></script>
<script src="<%=sessionBean.getRutaKitUI()%>/latest/semantic.min.js"></script>
<script src="<%=sessionBean.getRutaKitUI()%>/latest/components/tooltipster/js/tooltipster.bundle.js"></script>
<script src="<%=sessionBean.getRutaKitUI()%>/latest/components/diyslider.js"></script>
<script src="<%=sessionBean.getRutaKitUI()%>/latest/components/hc-offcanvas-nav.js"></script>
<script src="${pageContext.request.contextPath}/js/funciones.js"></script>

