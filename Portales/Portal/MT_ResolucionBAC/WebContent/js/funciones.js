function alerta(str){
	alert(str);
}

function destuyeTabla(){
	try{
		$('#tablaCentros').DataTable().destroy();
	} catch(ex){
		;
	}
}
function reconstruirTabla(){
	try{
		$('#tablaCentros').dataTable({
			dom : '<"top"f>rt<"bottom"lp><"clear">',
			language : {
				paginate : {
					previous : 'Anterior',
					next : 'Siguiente',
				},
				sLengthMenu : "Mostrando _MENU_ <span> de <span class='bold'><span class='cantDefaultStatic'></span></span> encontrados.</span>",
				zeroRecords : 'Sin resultados',
			},
			bPaginate : true,
			bInfo : false,
			ordering : false,
			destroy : true,
	
		});
	} catch(ex){
		;
	}
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function reconstruirEliminarRegistro(){
	 $('.accion-eliminar').click(function(){
		 var idCentro = $(this).attr('data-id');
		 arrEliminaCentro(idCentro);
		 destuyeTabla();
		 if(idCentro != ''){
			 $('tr[data-id='+idCentro+']').remove();
		 }
		 reconstruirTabla();
		 $('#centroTrabajoBD').parent().find('div[data-value='+idCentro+']').show();
	 });
}

function reconstruirEditarRegistro(){
	 $('.accion-editar').click(function(){
		 $('#centroTrabajoDetalle').show();
		 $('#centroTrabajoBD').val('0');
		 $('#centroTrabajoBD').parent().find('.text').html('- Otro -');
		 destuyeTabla();
		 var idCentro = $(this).attr('data-id');
		 $('#idCentroAccion').val(idCentro);
		 console.log('editar idCentro:'+idCentro);
		 var tr = $('tr[data-id='+idCentro+']');
		 var regionId = tr.attr('data-region');
		 console.log('editar region:'+regionId);
		 $('#idCentroAccion').val(idCentro);
		 $('#loadingwaitModal2').show();	

		var parametros = {
			'accion' : 'buscaComunas',
			'region' : regionId
		};
		$.ajax({
			data : parametros,
			url : resourceUrl,
			type : 'post',
			beforeSend : function() {
				console.log('enviando');
			},
			success : function(response) {
				if(response.codigo == 0){
					
					var tr = $('tr[data-id='+idCentro+']');
					$('#nombreCentro').val(tr.attr('data-nombreCentro'));
					$('#centroIdInterno').val(tr.attr('data-centroIdInterno'));
					$('#calle').val(tr.attr('data-calle'));
					$('#numero').val(tr.attr('data-numero'));
					$('#depto').val(tr.attr('data-depto'));
					$('#region').val(tr.attr('data-regionId'));
					$('#comuna').val(tr.attr('data-comunaId'));
					
					$('#opcionesComunas').html('');
					$.each( response.comunas, function( index, comuna ) {
						$('#opcionesComunas').append($('<div>').addClass('item').attr('data-value', comuna.id).html(comuna.valor));
					});
					$('#comuna').val(tr.attr('data-comunaId'));
					$('.ui.dropdown').dropdown({ showOnFocus:false });
					
					$('#agregarCentro').hide();
					$('#modificarCentro').show();
					$('#centroTrabajoDetalle').show();
			        $('#loadingwaitModal2').hide();
				} else {
					$('#loadingwaitModal2').hide();
				}
			}, error : function(response){
				$('#loadingwaitModal2').hide();
			}
		});
		reconstruirTabla();
		
		reconstruirEliminarRegistro();
		
		reconstruirEditarRegistro();
	 });
}

function getIdCentro(comunaId,calle,numero){
	var retorno = comunaId;
	retorno = retorno + calle;
	retorno = retorno + numero;
	
	limpiaCaracteres(retorno);
	retorno = retorno.replace(new RegExp(' ', 'g'), '');
	return retorno;
}

function limpiaCaracteres(retorno){
	retorno = retorno.replace(new RegExp(',', 'g'), '');
	retorno = retorno.replace(new RegExp('\\.', 'g'), '');
	retorno = retorno.replace(new RegExp('"', 'g'), '');
	retorno = retorno.replace(new RegExp('\'', 'g'), '');
	retorno = retorno.replace(new RegExp('#', 'g'), '');
	retorno = retorno.replace(new RegExp('$', 'g'), '');
	retorno = retorno.replace(new RegExp('|', 'g'), '');
	retorno = retorno.replace(new RegExp('/', 'g'), '');
	return retorno;
}

function formateaNumero(valor){
	return valor.replace(/[^0-9]/g,'');
}

$(document).ready(function(){
	
	 $('.ui.dropdown').dropdown({ showOnFocus:false });
	 
	 $.each(centrosIngresados, function( index, value ) {
		 if(value != ''){
			 $('#centroTrabajoBD').parent().find('div[data-value='+value+']').hide();
		 }
	 });
	 
	 $('.numerico').on('change keyup keypress', function(){
		$(this).val(formateaNumero($(this).val()));
	});
	 
	 reconstruirEliminarRegistro();
	 
	 reconstruirEditarRegistro();
	 
	 reconstruirTabla();
	 
	 $('#formularioBAC input').blur(function(){
		 var id = $(this).attr('id');
		 $('.error-formulario-'+id).hide();
	 });
	 
	 $('#comuna').change(function(){
		 $('.error-formulario-comuna').hide();
	 });
	 
	 
	 $('#region').change(function(){
		 $('.error-formulario-region').hide();
			$('#loadingwaitModal2').show();	
			var idRegion = $(this).val();

			var parametros = {
				'accion' : 'buscaComunas',
				'region' : idRegion
			};
			$.ajax({
				data : parametros,
				url : resourceUrl,
				type : 'post',
				beforeSend : function() {
					console.log('enviando');
				},
				success : function(response) {
					if(response.codigo == 0){
						$('#opcionesComunas').html('');
						$.each( response.comunas, function( index, comuna ) {
							$('#opcionesComunas').append($('<div>').addClass('item').attr('data-value', comuna.id).html(comuna.valor));
						});
						$('.ui.dropdown').dropdown({ showOnFocus:false });
						$('#comuna').val('');
						$('#comuna').parent().find('.text').html('');
				        $('#loadingwaitModal2').hide();
					} else {
						$('#loadingwaitModal2').hide();
					}
				}, error : function(response){
					$('#loadingwaitModal2').hide();
				}
			});
			
		});
	 
	 

	 
	 $('#ingresarCentroTrabajo').click(function(){
		 var centroTrabajoBD = $('#centroTrabajoBD').val();
		 	if(arrExisteCentro(centroTrabajoBD)){
		 		alerta('Ya está asignado el centro de trabajo');
		 	} else {
			 	if(centroTrabajoBD != '0'){
			 		$('#loadingwaitModal2').show();
					var parametros = {
						'accion' : 'agregaCentro',
						'centro' : centroTrabajoBD
					};
					$.ajax({
						data : parametros,
						url : resourceUrl,
						type : 'post',
						beforeSend : function() {
							console.log('enviando');
						},
						success : function(response) {
							if(response.codigo == 0){
								
								destuyeTabla();
							
								var tr = $('<tr>').attr('data-id', response.centro.centroId)
			 					.attr('data-centroIdInterno', response.centro.centroIdInterno)
			 					.attr('data-centroNuevo', 1)
								.attr('data-nombreCentro', response.centro.nombreCentro)
								.attr('data-calle', response.centro.calle)
								.attr('data-numero', response.centro.numero)
								.attr('data-depto', response.centro.depto)
								.attr('data-regionId', response.centro.regionId)
								.attr('data-region', response.centro.region)
								.attr('data-comunaId', response.centro.comunaId)
								.attr('data-comuna', response.centro.comuna)
								;
								
								
								var tdNombre = $('<td>').attr('data-label', 'nombre')
									.append($('<div>').addClass('titleTable').html('Nombre :'))
									.append($('<div>').addClass('tableData').html(response.centro.nombreCentro));
								var tdCalle = $('<td>').attr('data-label', 'calle')
									.append($('<div>').addClass('titleTable').html('Calle :'))
									.append($('<div>').addClass('tableData').html(response.centro.calle));
								var tdNumero = $('<td>').attr('data-label', 'numero')
									.append($('<div>').addClass('titleTable').html('Número :'))
									.append($('<div>').addClass('tableData').html(response.centro.numero));
								var tdDepto = $('<td>').attr('data-label', 'Depto/Oficina')
									.append($('<div>').addClass('titleTable').html('Depto/Oficina :'))
									.append($('<div>').addClass('tableData').html(response.centro.depto));
								var tdRegion = $('<td>').attr('data-label', 'Región')
									.append($('<div>').addClass('titleTable').html('Región :'))
									.append($('<div>').addClass('tableData').html(response.centro.region));
								var tdComuna = $('<td>').attr('data-label', 'Comuna')
									.append($('<div>').addClass('titleTable').html('Comuna :'))
									.append($('<div>').addClass('tableData').html(response.centro.comuna));
								
								var tdDAcciones = $('<td>')
									.append($('<div>').addClass('titleTable').html('Acción :'))
									.append($('<div>').addClass('tableData').append(
											$('<span>').addClass('acciones accion-eliminar')
												.attr('data-id', response.centro.centroId)
												.attr('data-tooltip', 'Eliminar')
												.attr('data-position', 'top center').append(
														$('<i>').addClass('icon far fa-trash link large')
												)
									));
								
							
								tr.append(tdNombre).append(tdCalle).append(tdNumero).append(tdDepto).append(tdRegion)
								.append(tdComuna).append(tdDAcciones);
								
								$('#tablaCentros tbody').append(tr);
								$('#centroTrabajoBD').parent().find('div[data-value='+response.centro.centroId+']').hide();
								$('#centroTrabajoBD').parent().find('.text').html('');
								$('#centroTrabajoBD').val('');
							
								reconstruirTabla();
							
								reconstruirEliminarRegistro();
								
						        $('#loadingwaitModal2').hide();
							} else {
								$('#loadingwaitModal2').hide();
							}
						}, error : function(response){
							$('#loadingwaitModal2').hide();
						}
					});
			 	}
		 	}
			
	 });
	 
	 $('#centroTrabajoBD').change(function(){
		 var centroTrabajoBD = $('#centroTrabajoBD').val();
		 if(centroTrabajoBD == '0'){
			 $('#centroIdInterno').val('');
			 $('#nombreCentro').val('');
			 $('#calle').val('');
			 $('#numero').val('');
			 $('#depto').val('');
			 $('#region').val('');
			 $('#region').parent().find('.text').html('');
			
			 $('#opcionesComunas').html('');			
			 $('#comuna').val('');
			 $('#comuna').parent().find('.text').html('');
			 
			 $('.ui.dropdown').dropdown({ showOnFocus:false });
			 $('#centroTrabajoDetalle').show();
		 } else {
			 $('#centroTrabajoDetalle').hide();
		 }
	 });
	 
	 function limpiarDivCentro(){
		 $('#idCentroAccion').val('');
		 $('#centroIdInterno').val('');
		 $('#nombreCentro').val('');
		 $('#calle').val('');
		 $('#numero').val('');
		 $('#depto').val('');
		 $('#region').val('');
		 $('#region').parent().find('.text').html('');
		 $('#opcionesComunas').html('');
		 $('#comuna').val('');
		 $('#comuna').parent().find('.text').html('');
		 $('#modificarCentro').hide();
		 $('#agregarCentro').show();
	 }
	 
	 $('#cancelarCentro').click(function(){
		 limpiarDivCentro();
	 });
	 
	 $('#modificarCentro').click(function(){
		 var idCentro = $('#idCentroAccion').val();
		 destuyeTabla();
		 if(idCentro != ''){
			 $('tr[data-id='+idCentro+']').remove();
		 }
		 agregarCentro();
	 });
	 
	 $('#agregarCentro').click(function(){
		 agregarCentro();
	 });	
	 
	 function agregarCentro(){
		 var centroIdInterno = $('#centroIdInterno').val();
 		 var nombreCentro = limpiaCaracteres($('#nombreCentro').val());
 		 var calle = limpiaCaracteres($('#calle').val());
 		 var numero = limpiaCaracteres($('#numero').val());
 		 var depto = limpiaCaracteres($('#depto').val());
 		 var regionId = $('#region').val();
 		 var region = $('#region').parent().find('.text').html();
 		 var comunaId = $('#comuna').val();
 		 var comuna = $('#comuna').parent().find('.text').html();
 		 var centroId = getIdCentro(comunaId, calle, numero);
 		 
 		 $('.error-formulario').hide();
 		 var continuar = true;
 		 if($.trim(nombreCentro) == ''){
 			 $('.error-formulario-nombreCentro').show();
 			continuar = false;
 		}
 		if($.trim(calle) == ''){
			 $('.error-formulario-calle').show();
			continuar = false;
		}
 		if($.trim(numero) == ''){
			 $('.error-formulario-numero').show();
			continuar = false;
		}
 		if($.trim(regionId) == ''){
			 $('.error-formulario-region').show();
			continuar = false;
		}
 		if($.trim(comunaId) == ''){
			 $('.error-formulario-comuna').show();
			continuar = false;
		}
 		if(!continuar){
 			alerta('Debe completar los campos obligatorios');
 		} else if(arrExisteCentro(centroId)){
 			alerta('Ya está asignado el centro de trabajo');
		} else {
			 $('#loadingwaitModal2').show();	
		 		destuyeTabla();
		 		var tr = $('<tr>').attr('data-id', centroId)
		 					.attr('data-centroIdInterno', centroIdInterno)
		 					.attr('data-centroNuevo', 2)
							.attr('data-nombreCentro', nombreCentro)
							.attr('data-calle', calle)
							.attr('data-numero', numero)
							.attr('data-depto', depto)
							.attr('data-regionId', regionId)
							.attr('data-region', region)
							.attr('data-comunaId', comunaId)
							.attr('data-comuna', comuna)
							;
			
			var tdNombre = $('<td>').attr('data-label', 'nombre')
				.append($('<div>').addClass('titleTable').html('Nombre :'))
				.append($('<div>').addClass('tableData').html(nombreCentro));
			var tdCalle = $('<td>').attr('data-label', 'calle')
				.append($('<div>').addClass('titleTable').html('Calle :'))
				.append($('<div>').addClass('tableData').html(calle));
			var tdNumero = $('<td>').attr('data-label', 'numero')
				.append($('<div>').addClass('titleTable').html('Número :'))
				.append($('<div>').addClass('tableData').html(numero));
			var tdDepto = $('<td>').attr('data-label', 'Depto/Oficina')
				.append($('<div>').addClass('titleTable').html('Depto/Oficina :'))
				.append($('<div>').addClass('tableData').html(depto));
			var tdRegion = $('<td>').attr('data-label', 'Región')
				.append($('<div>').addClass('titleTable').html('Región :'))
				.append($('<div>').addClass('tableData').html(region));
			var tdComuna = $('<td>').attr('data-label', 'Comuna')
				.append($('<div>').addClass('titleTable').html('Comuna :'))
				.append($('<div>').addClass('tableData').html(comuna));
			
		
			var tdDAcciones = $('<td>')
				.append($('<div>').addClass('titleTable').html('Acción :'))
				.append($('<div>').addClass('tableData')
						.append($('<span>').addClass('acciones accion-editar')
							.attr('data-id', centroId)
							.attr('data-tooltip', 'Editar')
							.attr('data-position', 'top center').append(
									$('<i>').addClass('icon far fa-edit link large')
							))
						.append($('<span>').addClass('acciones accion-eliminar')
							.attr('data-id', centroId)
							.attr('data-tooltip', 'Eliminar')
							.attr('data-position', 'top center').append(
									$('<i>').addClass('icon far fa-trash link large')
							))
				);
			
		
			tr.append(tdNombre).append(tdCalle).append(tdNumero).append(tdDepto).append(tdRegion)
			.append(tdComuna).append(tdDAcciones);
			
			$('#tablaCentros tbody').append(tr);
			$('#centroTrabajoBD').parent().find('.text').html('');
			$('#centroTrabajoBD').val('');
		
			reconstruirTabla();
		
			reconstruirEliminarRegistro();
			
			reconstruirEditarRegistro();
			
			limpiarDivCentro();
			
			$('#centroTrabajoDetalle').hide();
			
	        $('#loadingwaitModal2').hide();
		 }
	 }
	 
	 
	 $('#enviarCentros').click(function(){
		 $('.error-formulario-telefono').hide();
		 $('.error-formulario-correo').hide();
		 var telefono = $('#telefonoContacto').val();
		 var correo = $('#correoElectronico').val();
		 
		 var continuar = true;
		 if($.trim(telefono) == ''){
			 continuar = false;
			 $('.error-formulario-telefono').show();
		 }
		 if($.trim(correo) == ''){
			 continuar = false;
			 $('.error-formulario-correo').show();
		 }
		 if(!isEmail($.trim(correo))){
			 continuar = false;
			 $('.error-formulario-correo').show();
		 }
		 
		 
		 if(continuar){
			 $('#loadingwaitModal2').show();
			 destuyeTabla();
			 
			 var centrosFinales = '';
			 $('#tablaCentros tbody tr').each(function(){
				 var centroIdInterno = $(this).attr('data-centroIdInterno');
				 var centroId = $(this).attr('data-id');
				 var centroNuevo = $(this).attr('data-centroNuevo');
				 var nombreCentro = $(this).attr('data-nombreCentro');
				 var calle = $(this).attr('data-calle');
				 var numero = $(this).attr('data-numero');
				 var depto = $(this).attr('data-depto');
				 var region = $(this).attr('data-region');
				 var regionId = $(this).attr('data-regionId');
				 var comuna = $(this).attr('data-comuna');
				 var comunaId = $(this).attr('data-comunaId');
					
				 centrosFinales = centrosFinales 
				 		+ centroId + ','
				 		+ centroIdInterno + ','
				 		+ centroNuevo + ','
				 		+ nombreCentro + ','
				 		+ calle + ','
				 		+ numero + ','
				 		+ depto + ','
				 		+ region + ','
				 		+ regionId + ','
				 		+ comuna + ','
				 		+ comunaId + '|';
					
			 });
			 $('#centrosFinales').val(centrosFinales.toUpperCase());
			 $('#formularioBAC').submit();
		 }
	 });
	 
	
});