# Digitalización

## Generar WAR

Para generar el archivo WAR se debe ejecutar el siguiente comando.

* Linux/Mac `$ ./gradlew clean build`
* Windows `> gradlew clean build`

El war quedará en la siguente ruta **build/libs**

## Configuración Weblogic

Se debe crear la siguiente estructura de carpetas en el home del dominio weblogic
**resolucionbac/config**

### Archivo de configuración para el log

Se debe copiar el archivo **logback-resolucionbac.xml** en la siguiente ruta, **resolucionbac/config**,
el archivo se encuentra versionado en la siguiente ruta **src/main/resources/logback-resolucionbac.xml**

### Properties

Dentro de la carpeta config, se debe crear el siguiente archivo yml
**resolucionbac.yml**, con el siguiente contenido:
```
spring:
  datasource:
    jndi-name: cl.mutual.resolucionbac

store-procedure:
  schema:
  catalog:
  procedure: 
    get:
    put:
    post:

logging:
  config: file:${DOMAIN_HOME}/resolucionbac/config/logback-resolucionbac.xml
```
Se deben reemplazar los valores de las llaves **jndi-name**, **schema**, **catalog**, **get**, **put** y **post** por los que correspondan.

## Swagger

Se puede acceder al suagger del servicio con la siguiente URL.

`http://localhost:7001/resolucionbac/swagger-ui/`