package cl.mutual.resolucionbac.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SolicitudDTO implements Serializable {
    private long rutEmpresa;
    private String dvEmpresa;
    private String razonSocial;
    private long contrato;
    private long rutUsuario;
    private String dvUsuario;
    private String nombre;
    private String apePaterno;
    private String apeMaterno;
    private String telefono;
    private String correo;
    private String fechaEstado;
}
