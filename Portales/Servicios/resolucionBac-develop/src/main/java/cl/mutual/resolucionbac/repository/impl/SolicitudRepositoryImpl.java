package cl.mutual.resolucionbac.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.mutual.resolucionbac.dto.SolicitudDTO;
import cl.mutual.resolucionbac.repository.SolicitudRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class SolicitudRepositoryImpl implements SolicitudRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<SolicitudDTO> findAll(long numContrato, long rutUsuario) {
        log.info("SolicitudRepositoryImpl::findAll - START - Contrato: {} - Rut: {}", numContrato, rutUsuario);
        String query = "SELECT BAC_RUT AS RUT_EMPRESA, BAC_DV_RUT AS DV_EMPRESA, BAC_RAZON AS RAZON_SOCIAL, BAC_CONTRATO AS CONTRATO, CTT_RUT AS RUT_USUARIO, CTT_DV_RUT AS DV_USUARIO, CTT_NOMBRES AS NOMBRE, CTT_APELLIDO_PATERNO AS APEPATERNO, CTT_APELLIDO_MATERNO AS APEMATERNO, CTT_TELEFONO AS TELEFONO, CTT_EMAIL AS CORREO, CON_FECHA_ESTADO AS FECHA_ESTADO  FROM BAC_SOLICITUD WHERE CTT_RUT =  ?  AND BAC_CONTRATO = ?";

        List<SolicitudDTO> lista = jdbcTemplate.query(query,
                (rs, rowNm) -> new SolicitudDTO(
                        rs.getLong("RUT_EMPRESA"),
                        rs.getString("DV_EMPRESA"),
                        rs.getString("RAZON_SOCIAL"),
                        rs.getLong("CONTRATO"),
                        rs.getLong("RUT_USUARIO"),
                        rs.getString("DV_USUARIO"),
                        rs.getString("NOMBRE"),
                        rs.getString("APEPATERNO"),
                        rs.getString("APEMATERNO"),
                        rs.getString("TELEFONO"),
                        rs.getString("CORREO"),
                        rs.getString("FECHA_ESTADO")),
                new Object[] { rutUsuario, numContrato });
        log.info("SolicitudRepositoryImpl::findAll - END");
        return lista;
    }

    @Override
    public boolean save(SolicitudDTO solicitud) {
        log.info("SolicitudRepositoryImpl::save - START");
        log.debug("SolicitudRepositoryImpl::save - Solicitud: {}", solicitud);
        String query = "INSERT INTO BAC_SOLICITUD(BAC_RUT, BAC_DV_RUT, BAC_RAZON, BAC_CONTRATO, CTT_RUT,  CTT_DV_RUT, CTT_NOMBRES, CTT_APELLIDO_PATERNO, CTT_APELLIDO_MATERNO, CTT_TELEFONO, CTT_EMAIL, CON_FECHA_ESTADO ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, trunc(sysdate) )";
        log.info("SolicitudRepositoryImpl::save - END");
        return jdbcTemplate.update(query, new Object[] {
                solicitud.getRutEmpresa(),
                solicitud.getDvEmpresa(),
                solicitud.getRazonSocial(),
                solicitud.getContrato(),
                solicitud.getRutUsuario(),
                solicitud.getDvUsuario(),
                solicitud.getNombre(),
                solicitud.getApePaterno(),
                solicitud.getApeMaterno(),
                solicitud.getTelefono(),
                solicitud.getCorreo()
        }) > 0;
    }

    @Override
    public boolean update(SolicitudDTO solicitud, long numContrato, long rutUsuario) {
        log.info("SolicitudRepositoryImpl::update - START - Contrato: {} - Rut: {}", numContrato, rutUsuario);
        log.debug("SolicitudRepositoryImpl::update - Solicitud: {}", solicitud);
        String query = "UPDATE BAC_SOLICITUD SET BAC_RUT = ?, BAC_DV_RUT = ?, BAC_RAZON = ?, CTT_DV_RUT = ?, CTT_NOMBRES = ?, CTT_APELLIDO_PATERNO = ?, CTT_APELLIDO_MATERNO = ?, CTT_TELEFONO = ?, CTT_EMAIL = ?, CON_FECHA_ESTADO =  trunc(sysdate) WHERE BAC_CONTRATO = ?  AND CTT_RUT =  ?";
        log.info("SolicitudRepositoryImpl::update - END");
        return jdbcTemplate.update(query, new Object[] {
                solicitud.getRutEmpresa(),
                solicitud.getDvEmpresa(),
                solicitud.getRazonSocial(),
                solicitud.getDvUsuario(),
                solicitud.getNombre(),
                solicitud.getApePaterno(),
                solicitud.getApeMaterno(),
                solicitud.getTelefono(),
                solicitud.getCorreo(),
                numContrato,
                rutUsuario
        }) > 0;
    }

}
