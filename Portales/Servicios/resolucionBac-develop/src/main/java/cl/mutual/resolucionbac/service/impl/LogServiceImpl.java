package cl.mutual.resolucionbac.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.mutual.resolucionbac.repository.LogRepository;
import cl.mutual.resolucionbac.service.LogService;

@Service
public class LogServiceImpl implements LogService{

    @Autowired
    private LogRepository repositorio;
    @Override
    public boolean save(long contrato, long rut, String detalle) {
        return repositorio.save(contrato, rut, detalle);
    }
    
}
