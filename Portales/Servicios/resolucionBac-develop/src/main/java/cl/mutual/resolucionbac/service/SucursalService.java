package cl.mutual.resolucionbac.service;

import java.util.List;

import cl.mutual.resolucionbac.dto.SucursalDTO;

public interface SucursalService {
    public List<SucursalDTO> getListado(long numContrato, long rutUsuario);
    public List<SucursalDTO> getListado(long correlativo);
    public boolean guardar(SucursalDTO solicitud);
    public boolean actualizar(SucursalDTO solicitud, long correlativo);
    public SucursalDTO eliminar(long correlativo);
    public List<SucursalDTO> getListadoByContrato(long numContrato);
    
}
