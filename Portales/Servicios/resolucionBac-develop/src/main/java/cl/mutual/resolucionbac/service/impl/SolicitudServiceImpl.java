package cl.mutual.resolucionbac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.mutual.resolucionbac.dto.SolicitudDTO;
import cl.mutual.resolucionbac.repository.SolicitudRepository;
import cl.mutual.resolucionbac.service.SolicitudService;

@Service
public class SolicitudServiceImpl implements SolicitudService {

    @Autowired
    private SolicitudRepository repositorio;

    @Override
    public List<SolicitudDTO> getListado(long numContrato, long rutUsuario) {
        return repositorio.findAll(numContrato, rutUsuario);
    }

    @Override
    public boolean guardar(SolicitudDTO solicitud) {
        return repositorio.save(solicitud);
    }

    @Override
    public boolean actualizar(SolicitudDTO solicitud, long numContrato, long rutUsuario) {
        return repositorio.update(solicitud, numContrato, rutUsuario);
    }

}
