package cl.mutual.resolucionbac.controller;

import java.time.Clock;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.mutual.resolucionbac.config.Constants;
import cl.mutual.resolucionbac.dto.ActualizacionResponse;
import cl.mutual.resolucionbac.dto.SolicitudDTO;
import cl.mutual.resolucionbac.dto.SolicitudResponse;
import cl.mutual.resolucionbac.service.LogService;
import cl.mutual.resolucionbac.service.SolicitudService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping(value = "/api/v1/")
@Api(tags = {"Metodos"})
public class SolicitudController {
    private Clock clock = Clock.systemDefaultZone();

    @Autowired
    private SolicitudService service;

    @Autowired
    private LogService logService;

    @GetMapping(path = "/solicitud/{numContrato}/{rutUsuario}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SolicitudResponse> getSolicitudes(@PathVariable(value = "numContrato") long numContrato,@PathVariable(value = "rutUsuario") long rutUsuario) {
        log.info("SolicitudController::getSolicitudes - START - Contrato: {} - Rut: {}",numContrato, rutUsuario);
        long startTime = clock.millis();
        SolicitudResponse respuesta = new SolicitudResponse();
        try{
            List<SolicitudDTO> lista = service.getListado(numContrato, rutUsuario);
            respuesta.setSolicitudes(Optional.of(lista));
            respuesta.setCodigoError(Constants.CODIGO_EXITO);
            if(lista == null || lista.isEmpty()){
                respuesta.setMensajeError(Constants.MENSAJE_RESULTADO_VACIO);
                log.info("SolicitudController::getSolicitudes - {}", Constants.MENSAJE_RESULTADO_VACIO);
            }else{
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                log.info("SolicitudController::getSolicitudes - {}", Constants.MENSAJE_EXITO);
            }
        }catch(Exception e){
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SolicitudController::getSolicitudes - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() -startTime;
        log.info("Response: {}", respuesta);
        log.info("Elapsed execution time: {}", endTime);
        log.info("SolicitudController::getSolicitudes - END - Contrato: {} - Rut: {}",numContrato, rutUsuario);
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }


    @PostMapping(path = "/solicitud", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ActualizacionResponse> createSolicitud(@RequestBody SolicitudDTO request) {
        log.info("SolicitudController::createSolicitud - START");
        log.debug("SolicitudController::createSolicitud - Request {}", request);
        long startTime = clock.millis();
        ActualizacionResponse respuesta = new ActualizacionResponse();
        try{
            if(service.guardar(request)){
                respuesta.setCodigoError(Constants.CODIGO_EXITO);
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                logService.save(request.getContrato(), request.getRutUsuario(), Constants.MENSAJE_CREACION_SOLICITUD);
                log.info("SolicitudController::createSolicitud - {}", Constants.MENSAJE_EXITO);
            }else{
                respuesta.setCodigoError(Constants.CODIGO_ERROR);
                respuesta.setMensajeError(Constants.MENSAJE_ERROR);
                log.info("SolicitudController::createSolicitud - {}", Constants.MENSAJE_ERROR);
            }
        }catch(Exception e){
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SolicitudController::createSolicitud - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() -startTime;
        log.info("Elapsed execution time: {}", endTime);
        log.info("SolicitudController::createSolicitud - END");
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }

    @PutMapping(path = "/solicitud/{numContrato}/{rutUsuario}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ActualizacionResponse> updateSolicitud(
            @PathVariable(value = "numContrato") long numContrato,
            @PathVariable(value = "rutUsuario") long rutUsuario, 
            @RequestBody SolicitudDTO request) {
        log.info("SolicitudController::updateSolicitud - START - Contrato: {} - Rut: {}",numContrato, rutUsuario);
        log.debug("SolicitudController::updateSolicitud - Request {} ", request);
        long startTime = clock.millis();
        ActualizacionResponse respuesta = new ActualizacionResponse();
        try{
            if(service.actualizar(request, numContrato, rutUsuario)){
                respuesta.setCodigoError(Constants.CODIGO_EXITO);
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                logService.save(request.getContrato(), request.getRutUsuario(), Constants.MENSAJE_MODIFICACION_SOLICITUD);
                log.info("SolicitudController::updateSolicitud - {}", Constants.MENSAJE_EXITO);
            }else{
                respuesta.setCodigoError(Constants.CODIGO_ERROR);
                respuesta.setMensajeError(Constants.MENSAJE_ERROR);
                log.info("SolicitudController::updateSolicitud - {}", Constants.MENSAJE_ERROR);
            }
        }catch(Exception e){
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SolicitudController::updateSolicitud - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() -startTime;
        log.info("Elapsed execution time: {}", endTime);
        log.info("SolicitudController::updateSolicitud - END - Contrato: {} - Rut: {}",numContrato, rutUsuario);
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }
}
