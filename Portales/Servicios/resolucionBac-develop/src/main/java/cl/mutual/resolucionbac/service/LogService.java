package cl.mutual.resolucionbac.service;

public interface LogService {
    public boolean save(long contrato, long rut, String detalle);
}
