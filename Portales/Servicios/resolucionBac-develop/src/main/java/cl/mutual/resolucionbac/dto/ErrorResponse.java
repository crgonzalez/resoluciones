package cl.mutual.resolucionbac.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@JsonPropertyOrder({"timestamp", "status", "error", "details", "path"})
public class ErrorResponse {

    private LocalDateTime timestamp;
    private Integer status;
    private String error;
    private List<String> details;
    private String path;

}
