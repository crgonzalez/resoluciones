package cl.mutual.resolucionbac.service;

import java.util.List;

import cl.mutual.resolucionbac.dto.SolicitudDTO;

public interface SolicitudService {
    public List<SolicitudDTO> getListado(long numContrato, long rutUsuario);
    public boolean guardar(SolicitudDTO solicitud);
    public boolean actualizar(SolicitudDTO solicitud, long numContrato, long rutUsuario);
}
