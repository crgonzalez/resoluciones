package cl.mutual.resolucionbac.repository;

import java.util.List;

import cl.mutual.resolucionbac.dto.SucursalDTO;

public interface SucursalRepository {
    public List<SucursalDTO> findAll(long numContrato, long rutUsuario);
    public List<SucursalDTO> findAllByCorrelative(long correlativo);
    public boolean save(SucursalDTO sucursal);
    public boolean update(SucursalDTO sucursal, long correlativo);
    public SucursalDTO delete(long correlativo);
    public List<SucursalDTO> findAllByContrato(long numContrato);
}
