package cl.mutual.resolucionbac.dto;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class SolicitudResponse {
    public Optional<List<SolicitudDTO>> solicitudes;
    public int codigoError;
    public String mensajeError;
    public Date fechaConssulta = new Date(); 
}
