package cl.mutual.resolucionbac.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SucursalDTO {
    private long correlativo;
    private long contrato;
    private long rutUsuario;
    private String nombreCentro;
    private String calle;
    private String numero;
    private String oficina;
    private String region;
    private String ciudad;
    private String comuna;
    private String fechaEstado;
    private String estado;
}
