package cl.mutual.resolucionbac.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.mutual.resolucionbac.repository.LogRepository;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Repository
public class LogRepositoryImpl implements LogRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(long contrato, long rut, String detalle) {
        log.info("LogRepositoryImpl: Save - START - Contrato: {} - Rut: {} - Detalle: {}", contrato, rut, detalle);
        String query = "INSERT INTO BAC_SOLICITUD_LOG(BAC_NUM_LOG, BAC_CONTRATO, CTT_RUT, BAC_DETALLE, CON_FECHA) VALUES  (BAC_NUM_LOG_SEQ.NextVal, ?, ?, ? , trunc(sysdate))";
        log.info("LogRepositoryImpl: Save - END");
        return jdbcTemplate.update(query, new Object[] {contrato, rut, detalle}) > 0; 

    }

}
