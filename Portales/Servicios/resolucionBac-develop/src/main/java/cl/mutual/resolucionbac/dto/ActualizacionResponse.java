package cl.mutual.resolucionbac.dto;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class ActualizacionResponse {
    public int codigoError;
    public String mensajeError;
    public Date fechaConssulta = new Date(); 
}
