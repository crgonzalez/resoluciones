package cl.mutual.resolucionbac.repository;

import java.util.List;

import cl.mutual.resolucionbac.dto.SolicitudDTO;

public interface SolicitudRepository {
    public List<SolicitudDTO> findAll(long numContrato, long rutUsuario);
    public boolean save(SolicitudDTO solicitud);
    public boolean update(SolicitudDTO solicitud, long numContrato, long rutUsuario);
}
