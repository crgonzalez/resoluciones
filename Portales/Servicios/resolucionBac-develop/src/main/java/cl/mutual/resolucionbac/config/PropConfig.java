package cl.mutual.resolucionbac.config;

import org.springframework.boot.Banner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class PropConfig {

    private static final String CONFIG_PATH = "classpath:resolucionbac.yml,file:${DOMAIN_HOME}/config_mut/resolucionbac/";

    public SpringApplicationBuilder configure(SpringApplicationBuilder builder, Class clazz, String configFileName) {
        Properties properties = new Properties();
        properties.setProperty("spring.config.location", CONFIG_PATH + configFileName);
        return builder.properties(properties).sources(clazz).bannerMode(Banner.Mode.OFF);
    }

}
