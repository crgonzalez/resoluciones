package cl.mutual.resolucionbac.controller;

import java.time.Clock;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.mutual.resolucionbac.config.Constants;
import cl.mutual.resolucionbac.dto.ActualizacionResponse;
import cl.mutual.resolucionbac.dto.SucursalDTO;
import cl.mutual.resolucionbac.dto.SucursalResponse;
import cl.mutual.resolucionbac.service.LogService;
import cl.mutual.resolucionbac.service.SucursalService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1")
@Api(tags = { "Metodos" })
public class SucursalController {
    private Clock clock = Clock.systemDefaultZone();

    @Autowired
    private SucursalService service;

    @Autowired
    private LogService logService;

    @GetMapping(path = "/sucursal/{numContrato}/{rutUsuario}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SucursalResponse> getSucursales(
            @PathVariable(value = "numContrato") long numContrato,
            @PathVariable(value = "rutUsuario") long rutUsuario) {
        log.info("SucursalController::getSucursales - START - Contrato: {} - Rut: {}", numContrato, rutUsuario);
        long startTime = clock.millis();
        SucursalResponse respuesta = new SucursalResponse();
        try {
            List<SucursalDTO> lista = service.getListado(numContrato, rutUsuario);
            respuesta.setSucursales(Optional.of(lista));
            respuesta.setCodigoError(Constants.CODIGO_EXITO);
            if (lista == null || lista.isEmpty()) {
                respuesta.setMensajeError(Constants.MENSAJE_RESULTADO_VACIO);
                log.info("SucursalController::getSucursales - {}", Constants.MENSAJE_RESULTADO_VACIO);
            } else {
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                log.info("SucursalController::getSucursales - {}",Constants.MENSAJE_EXITO);
            }
        } catch (Exception e) {
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SucursalController::getSucursales - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() - startTime;
        log.info("Response: {}", respuesta);
        log.info("Elapsed execution time: {}", endTime);
        log.info("SucursalController::getSucursales - END - Contrato: {} - Rut: {}", numContrato, rutUsuario);
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }

    @GetMapping(path = "/sucursal/contrato/{numContrato}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SucursalResponse> getSucursalesByContrato(
            @PathVariable(value = "numContrato") long numContrato) {
        log.info("SucursalController::getSucursalesByContrato - START - Contrato: {}", numContrato);
        long startTime = clock.millis();
        SucursalResponse respuesta = new SucursalResponse();
        try {
            List<SucursalDTO> lista = service.getListadoByContrato(numContrato);
            respuesta.setSucursales(Optional.of(lista));
            respuesta.setCodigoError(Constants.CODIGO_EXITO);
            if (lista == null || lista.isEmpty()) {
                respuesta.setMensajeError(Constants.MENSAJE_RESULTADO_VACIO);
                log.info("SucursalController::getSucursalesByContrato - {}", Constants.MENSAJE_RESULTADO_VACIO);
            } else {
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                log.info("SucursalController::getSucursalesByContrato - {}",Constants.MENSAJE_EXITO);
            }
        } catch (Exception e) {
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SucursalController::getSucursalesByContrato - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() - startTime;
        log.info("Response: {}", respuesta);
        log.info("Elapsed execution time: {}", endTime);
        log.info("SucursalController::getSucursalesByContrato - END - Contrato: {}", numContrato);
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }

    @GetMapping(path = "/sucursal/{correlativo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SucursalResponse> getSucursalesCorrelativo(
            @PathVariable(value = "correlativo") long correlativo) {
        log.info("SucursalController::getSucursalesCorrelativo - START - Correlativo: {}", correlativo);
        long startTime = clock.millis();
        SucursalResponse respuesta = new SucursalResponse();
        try {
            List<SucursalDTO> lista = service.getListado(correlativo);
            respuesta.setSucursales(Optional.of(lista));
            respuesta.setCodigoError(Constants.CODIGO_EXITO);
            if (lista == null || lista.isEmpty()) {
                respuesta.setMensajeError(Constants.MENSAJE_RESULTADO_VACIO);
                log.info("SucursalController::getSucursalesCorrelativo - {}", Constants.MENSAJE_RESULTADO_VACIO);
            } else {
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                log.info("SucursalController::getSucursalesCorrelativo - {}",Constants.MENSAJE_EXITO);
            }
        } catch (Exception e) {
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SucursalController::getSucursalesCorrelativo - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() - startTime;
        log.info("Response: {}", respuesta);
        log.info("Elapsed execution time: {}", endTime);
        log.info("SucursalController::getSucursalesCorrelativo - START - Correlativo: {}", correlativo);
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }

    @PostMapping(path = "/sucursal", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ActualizacionResponse> createSucursal(@RequestBody SucursalDTO request) {
        log.info("SucursalController::createSucursal - START");
        log.debug("SucursalController::createSucursal - Request {} ", request);
        long startTime = clock.millis();
        ActualizacionResponse respuesta = new ActualizacionResponse();
        try {
            if (service.guardar(request)) {
                respuesta.setCodigoError(Constants.CODIGO_EXITO);
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                logService.save(request.getContrato(), request.getRutUsuario(), Constants.MENSAJE_CREACION_SUCURSAL);
                log.info("SucursalController::createSucursal - {}",Constants.MENSAJE_EXITO);
            } else {
                respuesta.setCodigoError(Constants.CODIGO_ERROR);
                respuesta.setMensajeError(Constants.MENSAJE_ERROR);
                log.info("SucursalController::createSucursal - {}",Constants.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SucursalController::createSucursal - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() - startTime;
        log.info("Elapsed execution time: {}", endTime);
        log.info("SucursalController::createSucursal - END");
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }

    @PutMapping(path = "/sucursal/{correlativo}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ActualizacionResponse> updateSucursal(@PathVariable(value = "correlativo") long correlativo,
            @RequestBody SucursalDTO request) {
        log.info("SucursalController::updateSucursal - START - Correlativo: {}", correlativo);
        log.debug("SucursalController::updateSucursal - Request {} ", request);
        long startTime = clock.millis();
        ActualizacionResponse respuesta = new ActualizacionResponse();
        try {
            if (service.actualizar(request, correlativo)) {
                respuesta.setCodigoError(Constants.CODIGO_EXITO);
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                logService.save(request.getContrato(), request.getRutUsuario(),Constants.MENSAJE_MODIFICACION_SUCURSAL);
                log.info("SucursalController::updateSucursal - {}",Constants.MENSAJE_EXITO);
            } else {
                respuesta.setCodigoError(Constants.CODIGO_ERROR);
                respuesta.setMensajeError(Constants.MENSAJE_ERROR);
                log.info("SucursalController::updateSucursal - {}",Constants.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SucursalController::updateSucursal - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() - startTime;
        log.info("Elapsed execution time: {}", endTime);
        log.info("SucursalController::updateSucursal - END - Correlativo: {}", correlativo);
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }

    @DeleteMapping(path = "/sucursal/{correlativo}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ActualizacionResponse> deleteSucursal(@PathVariable(value = "correlativo") long correlativo) {
        log.info("SucursalController::deleteSucursal - START - Correlativo: {}", correlativo);
        long startTime = clock.millis();
        ActualizacionResponse respuesta = new ActualizacionResponse();
        try {
            SucursalDTO resultado = service.eliminar(correlativo);
            if (resultado != null) {
                respuesta.setCodigoError(Constants.CODIGO_EXITO);
                respuesta.setMensajeError(Constants.MENSAJE_EXITO);
                logService.save(resultado.getContrato(), resultado.getRutUsuario(),
                        Constants.MENSAJE_ELIMINACION_SUCURSAL);
                log.info("SucursalController::deleteSucursal - Eliminado - Correlativo: {} - Contrato: {} - Rut: {} ",
                        correlativo, resultado.getContrato(), resultado.getRutUsuario());
            } else {
                respuesta.setCodigoError(Constants.CODIGO_ERROR);
                respuesta.setMensajeError(Constants.MENSAJE_ERROR);
                log.info("SucursalController::deleteSucursal - Registro no Encontrado - Correlativo: {} ", correlativo);
            }
        } catch (Exception e) {
            respuesta.setCodigoError(Constants.CODIGO_ERROR);
            respuesta.setMensajeError(e.getMessage());
            log.error("SucursalController::deleteSucursal - Exception: {} ", e.getMessage());
        }
        long endTime = clock.millis() - startTime;
        log.info("Elapsed execution time: {}", endTime);
        log.info("SucursalController::deleteSucursal - END - Correlativo: {}", correlativo);
        return ResponseEntity.status(HttpStatus.OK).body(respuesta);
    }

}
