package cl.mutual.resolucionbac.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.mutual.resolucionbac.dto.SucursalDTO;
import cl.mutual.resolucionbac.repository.SucursalRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class SucursalRepositoryImpl implements SucursalRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<SucursalDTO> findAll(long numContrato, long rutUsuario) {
        log.info("SucursalRepositoryImpl::findAll - START - Contrato: {} - Rut: {}", numContrato, rutUsuario);
        String sql = "SELECT BAC_NUM_CEN AS CORRELATIVO, BAC_CONTRATO AS CONTRATO, CTT_RUT AS RUT_USUARIO,  ADM_CENTRO AS NOMBRE_CENTRO, ADM_CALLE AS CALLE, ADM_NUMERO AS NUMERO, ADM_OFICINA AS OFICINA, ADM_REGION AS REGION, ADM_CIUDAD AS CIUDAD,ADM_COMUNA AS COMUNA, ADM_FECHA_ESTADO AS FECHA_ESTADO, ADM_ESTADO  AS ESTADO FROM BAC_SOLICITUD_CENTRO WHERE BAC_CONTRATO = ? AND CTT_RUT = ?";
        List<SucursalDTO> resultado = jdbcTemplate.query(
            sql,
            (rs, rowNm) -> new SucursalDTO(
                rs.getLong("CORRELATIVO"),
                rs.getLong(("CONTRATO")),
                rs.getLong("RUT_USUARIO"),
                rs.getString("NOMBRE_CENTRO"),
                rs.getString("CALLE"),
                rs.getString("NUMERO"),
                rs.getString("OFICINA"),
                rs.getString("REGION"),
                rs.getString("CIUDAD"),
                rs.getString("COMUNA"),
                rs.getString("FECHA_ESTADO"),
                rs.getString("ESTADO")
            ),
            new Object[]{numContrato, rutUsuario}
        );
        log.info("SucursalRepositoryImpl::findAll - END");
        return resultado;
    }

    @Override
    public List<SucursalDTO> findAllByCorrelative(long correlativo) {
        log.info("SucursalRepositoryImpl::findAllByCorrelative - START - Correlativo: {}", correlativo);

        String sql = "SELECT BAC_NUM_CEN AS CORRELATIVO, BAC_CONTRATO AS CONTRATO, CTT_RUT AS RUT_USUARIO,  ADM_CENTRO AS NOMBRE_CENTRO, ADM_CALLE AS CALLE, ADM_NUMERO AS NUMERO, ADM_OFICINA AS OFICINA, ADM_REGION AS REGION, ADM_CIUDAD AS CIUDAD,ADM_COMUNA AS COMUNA, ADM_FECHA_ESTADO AS FECHA_ESTADO, ADM_ESTADO  AS ESTADO FROM BAC_SOLICITUD_CENTRO WHERE BAC_NUM_CEN = ?";
        List<SucursalDTO> resultado = jdbcTemplate.query(
            sql,
            (rs, rowNm) -> new SucursalDTO(
                rs.getLong("CORRELATIVO"),
                rs.getLong(("CONTRATO")),
                rs.getLong("RUT_USUARIO"),
                rs.getString("NOMBRE_CENTRO"),
                rs.getString("CALLE"),
                rs.getString("NUMERO"),
                rs.getString("OFICINA"),
                rs.getString("REGION"),
                rs.getString("CIUDAD"),
                rs.getString("COMUNA"),
                rs.getString("FECHA_ESTADO"),
                rs.getString("ESTADO")
            ),
            new Object[]{correlativo}
        );
        log.info("SucursalRepositoryImpl::findAllByCorrelative - END");
        return resultado;
    }

    

    @Override
    public boolean save(SucursalDTO sucursal) {
        log.info("SucursalRepositoryImpl::save - START");
        log.debug("SucursalRepositoryImpl::save - Sucursal: {}", sucursal);
        String sql ="INSERT INTO BAC_SOLICITUD_CENTRO(BAC_NUM_CEN, BAC_CONTRATO, CTT_RUT, ADM_CENTRO, ADM_CALLE, ADM_NUMERO, ADM_OFICINA, ADM_REGION, ADM_CIUDAD, ADM_COMUNA, ADM_FECHA_ESTADO, ADM_ESTADO ) VALUES (BAC_NUM_CEN_SEQ.NextVal,?,?, ?, ?, ?, ?, ?, ?, ?, trunc(sysdate), 1)";
        boolean resultado = jdbcTemplate.update(sql, new Object[] {
            sucursal.getContrato(),
            sucursal.getRutUsuario(),
            sucursal.getNombreCentro(),
            sucursal.getCalle(),
            sucursal.getNumero(), 
            sucursal.getOficina(),
            sucursal.getRegion(),
            sucursal.getCiudad(),
            sucursal.getComuna()
        }) > 0; 
        log.info("SucursalRepositoryImpl::save - END");
        return resultado;
    }

    @Override
    public boolean update(SucursalDTO sucursal, long correlativo) {
        log.info("SucursalRepositoryImpl::update - START - Correlativo: {}", correlativo);
        log.debug("SucursalRepositoryImpl::update - Sucursal: {}", sucursal);
        String sql ="UPDATE BAC_SOLICITUD_CENTRO SET ADM_CENTRO = ?, ADM_CALLE = ?, ADM_NUMERO = ?, ADM_OFICINA = ?, ADM_REGION = ?, ADM_CIUDAD = ?,ADM_COMUNA = ?, ADM_FECHA_ESTADO = trunc(sysdate) , ADM_ESTADO  = 2 WHERE BAC_NUM_CEN = ?";
        boolean resultado = jdbcTemplate.update(sql, new Object[] {
            sucursal.getNombreCentro(),
            sucursal.getCalle(),
            sucursal.getNumero(), 
            sucursal.getOficina(),
            sucursal.getRegion(),
            sucursal.getCiudad(),
            sucursal.getComuna(),
            correlativo
        }) > 0; 
        log.info("SucursalRepositoryImpl::update - END");
        return resultado;
    }

    @Override
    public SucursalDTO delete(long correlativo) {
        log.info("SucursalRepositoryImpl::delete - START");
        SucursalDTO resultado = null;
        List<SucursalDTO> data = findAllByCorrelative(correlativo);
        if(data!=null && !data.isEmpty()){
            String sql = "DELETE BAC_SOLICITUD_CENTRO WHERE BAC_NUM_CEN = ?";
            if(jdbcTemplate.update(sql, new Object[] {correlativo}) > 0){
                resultado = data.get(0);
            }
        } 
        log.info("SucursalRepositoryImpl::delete - END");
        return  resultado;
    }

    @Override
    public List<SucursalDTO> findAllByContrato(long numContrato) {
        log.info("SucursalRepositoryImpl::findAllByContrato - START - Contrato: {}", numContrato);
        String sql = "SELECT BAC_NUM_CEN AS CORRELATIVO, BAC_CONTRATO AS CONTRATO, CTT_RUT AS RUT_USUARIO,  ADM_CENTRO AS NOMBRE_CENTRO, ADM_CALLE AS CALLE, ADM_NUMERO AS NUMERO, ADM_OFICINA AS OFICINA, ADM_REGION AS REGION, ADM_CIUDAD AS CIUDAD,ADM_COMUNA AS COMUNA, ADM_FECHA_ESTADO AS FECHA_ESTADO, ADM_ESTADO  AS ESTADO FROM BAC_SOLICITUD_CENTRO WHERE BAC_CONTRATO = ?";
        List<SucursalDTO> resultado = jdbcTemplate.query(
            sql,
            (rs, rowNm) -> new SucursalDTO(
                rs.getLong("CORRELATIVO"),
                rs.getLong(("CONTRATO")),
                rs.getLong("RUT_USUARIO"),
                rs.getString("NOMBRE_CENTRO"),
                rs.getString("CALLE"),
                rs.getString("NUMERO"),
                rs.getString("OFICINA"),
                rs.getString("REGION"),
                rs.getString("CIUDAD"),
                rs.getString("COMUNA"),
                rs.getString("FECHA_ESTADO"),
                rs.getString("ESTADO")
            ),
            new Object[]{numContrato}
        );
        log.info("SucursalRepositoryImpl::findAllByContrato - END");
        return resultado;
    }
    
}
