package cl.mutual.resolucionbac.config;

public class Constants {
    public static final int CODIGO_ERROR = 1;
    public static final int CODIGO_EXITO = 0;
    public static final String MENSAJE_ERROR = "Ejecucion No Exitosa";
    public static final String MENSAJE_EXITO = "Ejecucion Exitosa";
    public static final String MENSAJE_RESULTADO_VACIO = "Resultado Vacio";

    public static final String MENSAJE_CREACION_SOLICITUD ="NGRESA SOLICITUD";
    public static final String MENSAJE_CREACION_SUCURSAL ="INGRESA CENTRO DE TRABAJO";
    public static final String MENSAJE_MODIFICACION_SOLICITUD ="ACTUALIZA SOLICITUD";
    public static final String MENSAJE_MODIFICACION_SUCURSAL ="ACTUALIZA CENTRO DE TRABAJO";
    public static final String MENSAJE_ELIMINACION_SUCURSAL ="ELIMINA CENTRO DE TRABAJO";

}
