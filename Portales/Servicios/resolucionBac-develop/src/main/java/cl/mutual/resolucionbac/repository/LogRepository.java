package cl.mutual.resolucionbac.repository;

public interface LogRepository {
    public boolean save(long contrato, long rut, String detalle);
}
