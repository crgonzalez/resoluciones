package cl.mutual.resolucionbac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import cl.mutual.resolucionbac.config.PropConfig;

@SpringBootApplication
public class ResolucionBacApplication extends SpringBootServletInitializer {

	private static PropConfig CONFIG = new PropConfig();

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return CONFIG.configure(builder, ResolucionBacApplication.class, "resolucionbac.yml");
	}

	public static void main(String[] args) {
		SpringApplication.run(ResolucionBacApplication.class, args);
	}

}
