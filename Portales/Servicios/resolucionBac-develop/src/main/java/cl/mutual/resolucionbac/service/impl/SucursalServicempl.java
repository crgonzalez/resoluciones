package cl.mutual.resolucionbac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.mutual.resolucionbac.dto.SucursalDTO;
import cl.mutual.resolucionbac.repository.SucursalRepository;
import cl.mutual.resolucionbac.service.SucursalService;

@Service
public class SucursalServicempl implements SucursalService{

    @Autowired
    private SucursalRepository repositorio;

    @Override
    public List<SucursalDTO> getListado(long numContrato, long rutUsuario) {
        return repositorio.findAll(numContrato, rutUsuario);
    }

    @Override
    public boolean guardar(SucursalDTO sucursal) {
        return repositorio.save(sucursal);
    }

    @Override
    public boolean actualizar(SucursalDTO sucursal, long correlativo) {
        return repositorio.update(sucursal, correlativo);
    }

    @Override
    public List<SucursalDTO> getListado(long correlativo) {
        return repositorio.findAllByCorrelative(correlativo);
    }

    @Override
    public SucursalDTO eliminar(long correlativo) {
        return repositorio.delete(correlativo);
    }

    @Override
    public List<SucursalDTO> getListadoByContrato(long numContrato) {
        return repositorio.findAllByContrato(numContrato);
    }
    
}
