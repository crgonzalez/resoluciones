package cl.mutual.resolucionbac.exception;

public class StoreProcedureException extends RuntimeException {

    public StoreProcedureException() {
    }

    public StoreProcedureException(String message) {
        super(message);
    }

    public StoreProcedureException(String message, Throwable cause) {
        super(message, cause);
    }

    public StoreProcedureException(Throwable cause) {
        super(cause);
    }

    public StoreProcedureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
